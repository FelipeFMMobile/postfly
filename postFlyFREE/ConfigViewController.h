//
//  ConfigViewController.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 10/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialBinder.h"

@interface ConfigViewController : UIViewController <BinderDelegate>
@property (strong, nonatomic) IBOutlet UISwitch *_sFacebook;
@property (strong, nonatomic) IBOutlet UISwitch *_sTwitter;
@property (strong, nonatomic) IBOutlet UISwitch *_sLinkedin;
@property (strong, nonatomic) IBOutlet UISwitch *_sGooglePlus;

@property (strong, nonatomic) SocialBinder *socialB;


-(IBAction)selectSocial:(UISwitch *)sender;
-(IBAction)sair:(id)sender;
-(IBAction)openInfo:(id)sender;

@end
