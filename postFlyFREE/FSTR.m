//
//  Fstr.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 05/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "FSTR.h"



@implementation FSTR

#define LOCALIZATION_FILE @"InfoPlist"

+(NSString *)fstr :(NSString *) key {
    return NSLocalizedStringFromTable (key, LOCALIZATION_FILE, @"string localizada");

}

@end
