//
//  GoogleBinder.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 04/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Binder.h"



@class GPPSignIn;
//@class GooglePlusSignInButton;

//#import "GooglePlusShare.h"
#import "GPPSignIn.h"


@interface GoogleBinder : Binder <UIActionSheetDelegate>
#define GOOGLEP_ @"googleplus"

@property (nonatomic,strong) UIViewController * mainView;
@property (nonatomic,readonly) GTMOAuth2Authentication * mainAuth;
@property (nonatomic, strong) IBOutlet GTMOAuth2ViewControllerTouch * viewController;

- (id)init;

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error;

@end
