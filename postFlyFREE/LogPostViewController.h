//
//  LogPostViewController.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 11/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialBinder.h"

@interface LogPostViewController : UITableViewController {
    
}

@property (strong, nonatomic) SocialBinder *socialB;
@property (strong, nonatomic) NSArray *posts;
@property (strong, nonatomic) IBOutlet UITableViewCell *customCell1;

@end
