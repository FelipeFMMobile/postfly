//
//  MainViewController.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "MainViewController.h"
#import "FSTR.h"
#import <MobileCoreServices/MobileCoreServices.h> 
#import "ItePost.h"
#import "ConfigViewController.h"
#import "LogPostViewController.h"
#import "IAPPurchaseViewController.h"
#import "AppRater.h"
#import "GTMOAuth2ViewControllerTouch.h"


#define UIColorFromRGBA(rgbValue, alphaValue) \
[UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0x0000FF))/255.0 \
alpha:alphaValue]

static const CGRect kBallon1SpriteCoords = { .5, 0., .5, 1. };
static const CGRect kBallon2SpriteCoords = { 0., 0., .5, 1. };

@interface MainViewController (PrivateMethods)
// tipo = 0  kCATransitionPush, 1 kCATransitionMoveIn, 2 kCATransitionReveal, 3 kCATransitionFade
// direction = 0 kCATransitionFromRight, 1 kCATransitionFromLeft, 2 kCATransitionFromTop, 3 kCATransitionFromBottom
-  (CATransition *) setAnimType:(int)tipo :(int)direction :(float)timming;
-(BOOL) checkFirstTime;
-  (BOOL) entranceAnim;
-  (BOOL) flyAnimCoverSequence;
-  (BOOL) hideObjects;
-  (BOOL) finishCover;
-  (BOOL) flyAnimBallonSequence;
-  (BOOL) finishAnimOpenResult;
@end

@implementation MainViewController

@synthesize socialB;
@synthesize _profImage;
@synthesize _back_profimage;
@synthesize _postImage;
@synthesize _back_postimage;
@synthesize _labelConter;
@synthesize _labelProfName;
@synthesize _label_face;
@synthesize _label_twitter;
@synthesize _label_linkedin;
@synthesize _label_google;
@synthesize _labelFreeLimit;
@synthesize _label_uso;
@synthesize _postText;
@synthesize _flyButton;
@synthesize _pickImage;
@synthesize _getStore;
@synthesize _selFacebook;
@synthesize _selTwitter;
@synthesize _selLinkedin;
@synthesize _selGoogle;
@synthesize popoverController;
@synthesize _backSky;
@synthesize _spriteBallon;
@synthesize _labelFlyMessage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title=@"postFLY";
    first_time=NO;
    
    selFacebook=NO;
    selTwitter=NO;
    selGoogle=NO;
    selLinkedin=NO;
    UIBarButtonItem *cameraButton=nil;
     if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
         cameraButton = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                          target:self action:@selector(pickImage:)];
     } else {
         cameraButton = [[UIBarButtonItem alloc]
                                          initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                          target:self action:@selector(camera:)];
     }
    
    UIBarButtonItem *limparButton = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem:UIBarButtonSystemItemTrash
                                     target:self action:@selector(clearContent:)];
    
    UIBarButtonItem *configButton = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize
                                     target:self action:@selector(callConfig)];
    
    
    UIBarButtonItem *logButton = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks
                                     target:self action:@selector(callLog)];

    
    self.toolbarItems = [NSArray arrayWithObjects:
                         limparButton,configButton,logButton,cameraButton,
                         nil];
      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
         // [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"bar2_ipad.png"]  forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
      } else {
         // [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"bar2.png"]  forToolbarPosition:UIToolbarPositionBottom barMetrics:UIBarMetricsDefault];
      }
    
    
    
    [self clearContent:nil];
    
/*    if ([self.socialB isConnected]) {
        [self.socialB checkSign];
    }*/
    againCheckin=NO;
    isChecked=NO;
    // inicializa o contador e verificação de finalização de post.
    isFinished=YES;
    finish_conter=0;

    // passando a mainview controller para o objeto googleplus
    socialB.googleplus.mainView=self;
  
     [self entranceAnim];
    
    // implementa TapfotTap para Free users
    
    NSUserDefaults *defaults = [NSUserDefaults  standardUserDefaults];
    if (![defaults boolForKey:@"app_purchase"]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            // Show a banner at the bottom of this view, 320x50 points
            CGFloat y = self.view.frame.size.height - 100.0;
            TFTBanner *banner = [TFTBanner bannerWithFrame: CGRectMake(0, y, 320, 50) delegate: self];
            [self.view addSubview: banner];
        } else {
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if(iOSDeviceScreenSize.height >= 568){
                // Show a banner at the bottom of this view, 320x50 points
                CGFloat y = self.view.frame.size.height - 20.0;
                TFTBanner *banner = [TFTBanner bannerWithFrame: CGRectMake(0, y, 320, 50) delegate: self];
                [self.view addSubview: banner];
            }
        }
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    [self.socialB setDelegateForBinders:self];
    
    if ([self checkFirstTime]) {
        return;
    }
    
    if (!isChecked) {
        if ([self.socialB isConnected]) {
            [self.socialB checkSign];
            isChecked=YES;
             [self clearContent:nil];
        } else {
            return;
        }
    } else {
         if (![self.socialB isConnected])
             isChecked=NO;
    }
  
    if (first_time) {
        [self clearContent:nil];
        first_time=NO;
    }
    [self.navigationController setToolbarHidden:NO animated:YES];
    //  self.navigationController.toolbarHidden=NO;
    //[self.navigationController.toolbar.layer addAnimation:[self setAnimType:0 :2:.5] forKey:nil];
    
    if (self._postImage.image==nil) {
        self._pickImage.hidden=NO;
    }
    [self fechar];

}


-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
 
}

-(void)setAgainCheckinl:(BOOL)flag {
    againCheckin=flag;
}


-(BOOL) checkFirstTime {
    
    NSUserDefaults *defaults = [NSUserDefaults  standardUserDefaults];
    
        if ([defaults valueForKey:@"first"]==nil) {
            [self callConfig];
                first_time=YES;
                return YES;
        } 

    return NO;
}


-(IBAction)selectSocial:(id)select {
    
    if (select==self._selFacebook && [self.socialB.facebook isUse]) {
        selFacebook=!selFacebook;
         [self._selFacebook setOn:selFacebook];
    } 
     if (select==self._selFacebook && ![self.socialB.facebook isUse]) {
         [self._selFacebook setOn:NO];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_notconfig"]
                                                         message:[FSTR fstr:@"alert_mens_notconfig"]
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
         [self setAgainCheckinl:YES];
         [self callConfig];
     }
    if (select==self._selTwitter && [self.socialB.twitter isUse]) {
        selTwitter=!selTwitter;
        [self._selTwitter setOn:selTwitter];
    } 
    if (select==self._selTwitter && ![self.socialB.twitter isUse]) {
        [self._selTwitter setOn:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_notconfig"]
                                                        message:[FSTR fstr:@"alert_mens_notconfig"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
         [self setAgainCheckinl:YES];
        [self callConfig];
    }
    if (select==self._selLinkedin && [self.socialB.linkedin isUse]) {
        selLinkedin=!selLinkedin;
        [self._selLinkedin setOn:selLinkedin];
    } 
     if (select==self._selLinkedin && ![self.socialB.linkedin isUse]) {
        [self._selLinkedin setOn:NO];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_notconfig"]
                                                         message:[FSTR fstr:@"alert_mens_notconfig"]
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
         [alert show];
         [self setAgainCheckinl:YES];
         [self callConfig];

    }
    if (select==self._selGoogle && [self.socialB.googleplus isUse]) {
        if (image_post==nil) {
            selGoogle=!selGoogle;
            [self._selGoogle setOn:selGoogle];
        }
    }
    if (select==self._selGoogle && ![self.socialB.googleplus isUse]) {
        [self._selGoogle setOn:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_notconfig"]
                                                        message:[FSTR fstr:@"alert_mens_notconfig"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        [self setAgainCheckinl:YES];
        [self callConfig];
        
        if (self._postImage.image!=nil) {
            if (!selFacebook && selLinkedin) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_linkimg"]
                                                                message:[FSTR fstr:@"alert_mens_linkimg"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                selLinkedin=NO;
                [self._selLinkedin setOn:NO];
            }
        }
        
    }
}

-(IBAction)clearContent:(id)sender{
    
    image_post=nil;
    image_ref=nil;
    
    [self._selFacebook setOn:NO];
    [self._selTwitter setOn:NO];
    [self._selLinkedin setOn:NO];
    [self._selGoogle setOn:NO];
    
    
    if ([self.socialB.facebook isUse]) {
        selFacebook=YES;
        [self._selFacebook setOn:YES];
        
    }
    
    if ([self.socialB.googleplus isUse]) {
        selGoogle=YES;
        [self._selGoogle setOn:YES];
    }
    
    if ([self.socialB.twitter isUse]) {
        selTwitter=YES;
        [self._selTwitter setOn:YES];
    }
    
    if ([self.socialB.linkedin isUse]) {
        selLinkedin=YES;
        [self._selLinkedin setOn:YES];
    }
    
    self._labelConter.text=@"";

    self._postText.text=@"";
    self._postImage.image=nil;
    self._back_postimage.hidden=YES;
    self._postImage.hidden=YES;
    self._pickImage.hidden=NO;
    self._label_uso.hidden=NO;
    
    [self.navigationController setToolbarHidden:NO animated:YES];
}

-(IBAction)getFull:(id)sender {
    IAPPurchaseViewController * iappView = [[IAPPurchaseViewController alloc] initWithNibName:@"IAPPurchaseView" bundle:nil];
    [self presentModalViewController:iappView animated:YES];
    

}

-(IBAction)sendPostFly:(id)sender {
    
    if (self._postImage.image!=nil) {
        if (!selFacebook && selLinkedin) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_linkimg"]
                                                            message:[FSTR fstr:@"alert_mens_linkimg"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            selLinkedin=NO;
            [self._selLinkedin setOn:NO];
            return;
        }
    }
    
    if (![self._postText.text length]>0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_textempty"]
                                                        message:[FSTR fstr:@"alert_mens_textempty"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (![self.socialB isConnected]) {
        [self setAgainCheckinl:YES];
     return;
    }
    
    // bloqueia o envio do google+ quando tiver imagem
    if (image_post!=nil) {
        selGoogle=NO;
        [self._selGoogle setOn:selGoogle];
    }
    
    // Post limitation control
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // checkin the day.
    self._labelFreeLimit.text=@"";
    if (![defaults boolForKey:@"app_purchase"]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSDate * last_day=[defaults valueForKey:@"last_day"];
        NSDate * hoje = [NSDate date];
        if (last_day==nil) last_day=hoje;
        [dateFormatter setDateFormat:@"dd"];
        int dias_hoje = [[dateFormatter stringFromDate:hoje] intValue];
        int dias_ultimo = [[dateFormatter stringFromDate:last_day] intValue];
        if (dias_hoje!=dias_ultimo) {
            [defaults setFloat:3 forKey:@"free_limit"];
 
            [defaults synchronize];
        }
        float total = [[defaults valueForKey:@"free_limit"] floatValue];
        if (total<=0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_limit"]
                                                            message:[FSTR fstr:@"alert_mens_limit"]
                                                           delegate:nil
                                                           cancelButtonTitle:[FSTR fstr:@"bt_Cancel"]
                                                          otherButtonTitles:@"Ok", nil];
            [alert setDelegate:self]; 
            [alert show];
            return;
        }
        if (total>0) total=total-1;
        [defaults setFloat:total forKey:@"free_limit"];
        [defaults setValue:hoje forKey:@"last_day"];
        [defaults synchronize];
        self._labelFreeLimit.text=[NSString stringWithFormat:@"%@",[FSTR fstr:@"mens_free_limit"]];
    } // not purchase
    
    facebook_with_image=NO;
   
    NSString * message=[self._postText.text copy];
    NSMutableArray * redes = [[NSMutableArray alloc] init];
    temp_message=[self._postText.text copy];
    
    if (selFacebook) {
        if (image_post!=nil) facebook_with_image=YES;
        [redes addObject:@"facebook"];
        finish_conter++;
    }
    
    if (selTwitter) {
        [redes addObject:@"twitter"];
        finish_conter++;
    }
       
   if (selLinkedin) {
        [redes addObject:@"linkedin"];
         finish_conter++;
    }
    if (selGoogle) {
       [redes addObject:@"googleplus"];
        self.socialB.googleplus.mainView=self;
        if (self.socialB.googleplus.viewController==nil) {
        }
       finish_conter++;
    }
  self.socialB.image_ref_url=image_ref;
  if ([self.socialB postFly:nil :message :nil:redes :image_post]) {
     
      // contador para sugestao de rate do usuario
      if ([AppRater conterUsage]) {
          alert_rate=YES;
          [AppRater setRateMessage:[FSTR fstr:@"rate_message"]];
          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_rater"]
                                                          message:[AppRater getRateMessage]
                                                         delegate:self
                                                cancelButtonTitle:[FSTR fstr:@"bt_Cancel"]
                                                otherButtonTitles:@"OK", nil];
          [alert show];
      }
      isFinished=NO;
      if (![defaults boolForKey:@"app_purchase"]) {
          [self flyAnimCoverSequence];
      } else {
          [self finishAnimOpenResult];
      }
   } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_reconnect"]
                                                        message:[FSTR fstr:@"alert_mens_reconnect"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        if ([self.socialB isConnected]) {
            [self.socialB checkSign];
        }
    }
}

//delegate para abrir compra da app.
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0)
	{
        if (alert_rate) {
          alert_rate=NO;
            return;
        }
	}
	else if (buttonIndex == 1)
	{
        if (alert_rate) {
            [AppRater callReview];
            alert_rate=NO;
           
            return;
        }
		[self getFull:nil];
        
	}
}

-  (BOOL) entranceAnim {
    // anim para cor de borda.
   
    [self.navigationController.navigationBar.layer addAnimation:[self setAnimType:1 :3 :0.5] forKey:nil];
    
    //self._postText.layer.cornerRadius=10.;
    self._postText.layer.borderWidth = 4.;
    self._postText.layer.borderColor = [[UIColor orangeColor] CGColor];
    
  /*  [self._postText.layer addAnimation:[self setAnimType:2 :1:1.5] forKey:nil];
    
    
    
    CABasicAnimation *bcolorAnim = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    bcolorAnim.duration = 1.f;
    bcolorAnim.fillMode = kCAFillModeForwards;
    bcolorAnim.toValue = (id)[UIColorFromRGBA(0x0000FF, .75) CGColor];
    
    CABasicAnimation *bcolorAnim2 = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    bcolorAnim2.duration = 1.f;
    bcolorAnim2.fillMode = kCAFillModeForwards;
    bcolorAnim2.toValue = (id)[UIColorFromRGBA(0xFF0000, .75) CGColor];
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = [NSArray arrayWithObjects:bcolorAnim, bcolorAnim2, nil];
    group.duration = 1.;
    group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    group.autoreverses = YES;
    group.repeatCount = FLT_MAX;
    
    [self._postText.layer addAnimation:group forKey:@"current"];*/
    
    [self._selFacebook.layer addAnimation:[self setAnimType:0 :0:.5 ] forKey:nil];
    [self._selTwitter.layer addAnimation:[self setAnimType:0 :0:.7 ] forKey:nil];
    [self._selLinkedin.layer addAnimation:[self setAnimType:0 :0:.8 ] forKey:nil];
    [self._selGoogle.layer addAnimation:[self setAnimType:0 :0:.9 ] forKey:nil];
    
    [self._flyButton.layer addAnimation:[self setAnimType:0 :2:2.0 ] forKey:nil];
    
    return YES;
}

-  (BOOL) flyAnimCoverSequence {
   // hide navigators
    [self.navigationController setToolbarHidden:YES animated:YES];

  //  [self._coverImg.layer addAnimation:[self setAnimType:1 :2 :.5] forKey:@"cover"];
   // [self._coverImg.layer addAnimation:[self setAnimType:1 :2 :.5] forKey:@"cover"];
    
    float distance=-460;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        distance=-1000;
     }
    
   float start_conter=1.0;
   float duration=.5;
   float total_duration=start_conter+duration;
   /*  CABasicAnimation *moveIn = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    moveIn.duration = duration;
    moveIn.beginTime=start_conter;
    moveIn.fillMode = kCAFillModeForwards;
    moveIn.removedOnCompletion=NO;
    moveIn.fromValue=[NSNumber numberWithFloat:0];
    moveIn.toValue=[NSNumber numberWithFloat:distance];

    start_conter+=duration;
    CABasicAnimation *moveOut = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    moveOut.duration = duration;
    moveOut.beginTime=start_conter;
    moveOut.fillMode = kCAFillModeForwards;
    moveIn.removedOnCompletion=NO;
    moveOut.fromValue=[NSNumber numberWithFloat:distance];
    moveOut.toValue=[NSNumber numberWithFloat:0];
    
    float total_duration=start_conter+duration;
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.animations = [NSArray arrayWithObjects:moveIn, moveOut, nil];
    group.duration = total_duration;
    //group.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    group.removedOnCompletion = NO;
    group.fillMode = kCAFillModeForwards;
    //group.autoreverses = NO;
    //group.repeatCount = 0;
    [self._coverImg.layer addAnimation:group forKey:nil];*/
    //Queue up a timer to do cleanup once the group animation is finished.
    [NSTimer scheduledTimerWithTimeInterval: duration
                                     target: self
                                   selector: @selector(hideObjects)
                                   userInfo: nil
                                    repeats: NO];
    
    [NSTimer scheduledTimerWithTimeInterval: total_duration
                                     target: self
                                   selector: @selector(finishCover)
                                   userInfo: nil
                                    repeats: NO];
    
    return YES;
}

-  (BOOL) flyAnimBallonSequence {
    
   
    self._spriteBallon.hidden=NO;
    
    
    self._labelFreeLimit.hidden=NO;
    
    
   
    [self._labelFreeLimit.layer addAnimation:[self setAnimType:1 :1 :1] forKey:nil];
    
    self._labelFlyMessage.hidden=NO;
    [self._labelFlyMessage.layer addAnimation:[self setAnimType:1 :1 :1] forKey:nil];
    
    
    self._getStore.hidden=NO;
    
    CGMutablePathRef flyPath = CGPathCreateMutable();
     
    float baseX=-68;
    float baseY=350;
    float adjustY=0;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        adjustY=300;
      }
    float duration=3.0;
    if (image_post!=nil) {
        duration=6.0;
    }
    
    CGPathMoveToPoint(flyPath, NULL, baseX, baseY+adjustY);
    CGPathAddCurveToPoint(flyPath, NULL, -28.,223.+adjustY, 42., 356+adjustY, 90., 256.+adjustY);
    
    CGPathAddCurveToPoint(flyPath, NULL, 124., 136.+adjustY, 195., 267.+adjustY, 245., 170.+adjustY);
    CGPathAddLineToPoint(flyPath, NULL, 400, -100+adjustY);
     if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        CGPathAddLineToPoint(flyPath, NULL, 800, -200+adjustY);
     }
    
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.path = flyPath;
    pathAnimation.duration = duration;
    pathAnimation.delegate=self;
    
    [self._spriteBallon.layer addAnimation:pathAnimation forKey:@"ballon_anim"];
    
     CGPathRelease(flyPath);
    
    [NSTimer scheduledTimerWithTimeInterval: duration
                                     target: self
                                   selector: @selector(finishAnimOpenResult)
                                   userInfo: nil
                                    repeats: NO];
     
    return YES;
}

-  (BOOL) finishAnimOpenResult {

    NSManagedObject * post=[self.socialB getRecentPost];
    NSArray * itens = [[post valueForKey:@"itepost"] allObjects];
    for (NSString * item  in itens) {
        NSString * rede = [item valueForKey:@"rede_nome"];
        if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
            if (!facebook_with_image) {
                [self.socialB.googleplus postWall:temp_message :temp_message :nil];
            }
        }
    }

    self._backSky.hidden=YES;
    self._spriteBallon.hidden=YES;
    self._labelFlyMessage.hidden=YES;
     self._labelFreeLimit.hidden=YES;
      self._getStore.hidden=YES;
    [self._backSky.layer removeAllAnimations];
    [self._spriteBallon.layer removeAllAnimations];
    [self._labelFlyMessage.layer removeAllAnimations];
    self._postText.hidden=NO;
    self._profImage.hidden=NO;
    self._back_profimage.hidden=NO;

    self._postImage.hidden=NO;
    self._selFacebook.hidden=NO;
    self._selGoogle.hidden=NO;
    self._selLinkedin.hidden=NO;
    self._selTwitter.hidden=NO;
    self._label_face.hidden=NO;
    self._label_twitter.hidden=NO;
    self._label_linkedin.hidden=NO;
    self._label_google.hidden=NO;
    self._labelConter.hidden=NO;
         self._label_uso.hidden=NO;
    self._labelProfName.hidden=NO;
    self._flyButton.hidden=NO;
     [self.navigationController setNavigationBarHidden:NO animated:YES];
     [self clearContent:nil];
    
    // chamada tela de resultado
    [self callLog];
    
    return YES;
}

-(BOOL)hideObjects {
    self._postText.hidden=YES;
    self._profImage.hidden=YES;
    self._postImage.hidden=YES;
    self._pickImage.hidden=YES;
    self._selFacebook.hidden=YES;
    self._selGoogle.hidden=YES;
    self._selLinkedin.hidden=YES;
    self._selTwitter.hidden=YES;
    self._labelConter.hidden=YES;
    self._labelProfName.hidden=YES;
     self._label_uso.hidden=YES;
    self._flyButton.hidden=YES;
    self._back_profimage.hidden=YES;
    self._back_postimage.hidden=YES;
    self._label_face.hidden=YES;
    self._label_twitter.hidden=YES;
    self._label_linkedin.hidden=YES;
    self._label_google.hidden=YES;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self._backSky.hidden=NO;
    [self._backSky.layer addAnimation:[self setAnimType:3 :0 :.5] forKey:nil];
    
    return YES;
}
-  (BOOL) finishCover {
    // self._coverImg.hidden=YES;
    // [self._coverImg.layer removeAllAnimations];
     [self flyAnimBallonSequence];
    return YES;
}

-(void)callConfig {
  isChecked=NO;
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    ConfigViewController * config = [[ConfigViewController alloc] initWithNibName:@"ConfigView-iPad" bundle:nil];
    config.socialB=self.socialB;
    [self.navigationController pushViewController:config animated:YES];
  } else {
      ConfigViewController * config = [[ConfigViewController alloc] initWithNibName:@"ConfigView" bundle:nil];
      config.socialB=self.socialB;
      [self.navigationController pushViewController:config animated:YES];
  }
    
}

-(void)callLog {
  
    LogPostViewController * logpost = [[LogPostViewController alloc] initWithNibName:@"LogPostView" bundle:nil];
    logpost.socialB=self.socialB;
    logpost.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
      [self.navigationController pushViewController:logpost animated:YES];
    
}


- (CATransition *) setAnimType:(int) tipo :(int)direction :(float)timming {
    CATransition *transition = [CATransition animation];
    transition.duration = timming;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    NSString *transitionTypes[4] = { kCATransitionPush, kCATransitionMoveIn, kCATransitionReveal, kCATransitionFade };
    
    transition.type = transitionTypes[tipo];
    
    NSString *transitionSubtypes[4] = { kCATransitionFromRight, kCATransitionFromLeft, kCATransitionFromTop, kCATransitionFromBottom };
    
    transition.subtype = transitionSubtypes[direction];
    return transition;
}

#pragma delegate metodos

- (void)didPostImageWithUrl :(BOOL) sucess :(NSString *)url {
    if (facebook_with_image) {
        // completa a acao para outras midias sociais, que nao tem como fazer upload de foto.
        [self.socialB.facebook postWall:temp_message :temp_message :url];

        NSManagedObject * post=[self.socialB getRecentPost];
        
        NSArray * itens = [[post valueForKey:@"itepost"] allObjects];
        
        for (NSString * item  in itens) {
            
        NSString * rede = [item valueForKey:@"rede_nome"];

            if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
                if (facebook_with_image) {
                    [self.socialB.linkedin postWall:nil:temp_message :url];
                }
            }
            if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
                if (facebook_with_image) {
                    [self.socialB.googleplus postWall:temp_message :temp_message :url];
                }
            }
           
        } //for
       facebook_with_image=NO;
    }
}

-(void)didGetProfile:(BOOL)sucess :(NSString *)socialID {
     
    if ([socialID isEqualToString:@"facebook"]) {
        if (sucess) {
            if (self.socialB.facebook.profile_image!=nil) {
                self._profImage.image=self.socialB.facebook.profile_image;
                 
                [_profImage.layer addAnimation:[self setAnimType:3 :1:.5] forKey:nil];
                
                }
            self._labelProfName.text=self.socialB.facebook.profile_name;
        }
    }
}


-(void)didPost:(BOOL)sucess :(NSString *)socialID {
    
   
    NSManagedObject * post=[self.socialB getRecentPost];
    
    NSArray * itens = [[post valueForKey:@"itepost"] allObjects];
    
    for (NSString * item  in itens) {
        
        NSString * rede = [item valueForKey:@"rede_nome"];
        
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
              if ([@"facebook" rangeOfString:socialID].location != NSNotFound) {
                     [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
                  finish_conter--;
              }
        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            if ([@"twitter" rangeOfString:socialID].location != NSNotFound) {
                    [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
                finish_conter--;
            }
        }
        if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
            if ([@"linkedin" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
                finish_conter--;
            }
        }
        if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
            if ([@"googleplus" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
                   finish_conter--;
            }
        }
    }
    
    if (finish_conter<=0) {
        finish_conter=0;
        isFinished=YES;
    }
    
    
    
    NSError *error = nil;
    if (![self.socialB.managedObjectContext save:&error]) {
       // NSLog(@"Error %@",error);
    } 
  //  NSLog(@"Post em %@ realizado %i",socialID,sucess);
}


#pragma textfield handle 

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
        [self fechar];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
          
   
    [self verifyTextPost:textView];
     
}

-(void)verifyTextPost:(UITextView *)textView {
    self._labelConter.text=[NSString stringWithFormat:@"%i",[textView.text length]];
    if (![self.socialB postVerify:textView.text]) {
        self._labelConter.textColor=[UIColor redColor];
        selTwitter=false;
        [self._selTwitter setOn:NO];
    } else {
        self._labelConter.textColor=[UIColor grayColor];
    }

}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self animateTextField:textView up:YES];
     
}



-(void)fechar {
    self._postText.text=[self.socialB callShortner:self._postText.text];
    [self verifyTextPost:self._postText];
    
    [self._postText resignFirstResponder];


}


- (void)textViewDidEndEditing:(UITextView *)textView
{
   [self animateTextField:textView up:NO];
}

-(void)animateTextField:(UITextView*)textView up:(BOOL)up
{
    
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
        const int movementDistance = -80; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        
        int movement = (up ? movementDistance : -movementDistance);
        
        [UIView beginAnimations: @"animateTextField" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

#pragma ImagePicker 
-(IBAction)camera:(id)sender {
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
//    BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    // We are using an iPad
    imagePickerController.delegate = self;
    imagePickerController.sourceType  = UIImagePickerControllerSourceTypeCamera;
    [self presentModalViewController:imagePickerController animated:YES];
}

-(IBAction)pickImage:(id)sender {
    [[UIBarButtonItem appearance] setTintColor:[UIColor brownColor]];
    [self._postText resignFirstResponder];
       [self.navigationController setToolbarHidden:YES animated:YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // We are using an iPhone
        UIActionSheet *alertSheet = [[UIActionSheet alloc] initWithTitle:[FSTR fstr:@"alert_pickimage"] delegate:(self) cancelButtonTitle:[FSTR fstr:@"bt_Cancel"]  destructiveButtonTitle:nil otherButtonTitles:[FSTR fstr:@"tCamera"], [FSTR fstr:@"tLibrary"], nil];
        [alertSheet setTag:0];
        [alertSheet setDelegate:self];
        [alertSheet showInView:self.view];
    }else {
        // We are using an iPad
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
         imagePickerController.sourceType  = UIImagePickerControllerSourceTypePhotoLibrary;
        popoverController=[[UIPopoverController alloc] initWithContentViewController:imagePickerController];
        popoverController.delegate=self;
        [popoverController  presentPopoverFromRect:self._pickImage.bounds inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    
    imagePickerController.allowsEditing    = YES;
    imagePickerController.delegate          = self;
    
    if(buttonIndex == 0)
    {
        if (hasCamera) 
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else if(buttonIndex == 1)
    {
        
       imagePickerController.sourceType  = UIImagePickerControllerSourceTypePhotoLibrary;
   
    }
    if(buttonIndex == 2) {
        //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
          [self.navigationController setToolbarHidden:NO animated:YES];
    }
    if(buttonIndex != 2) {
              [self presentModalViewController:imagePickerController animated:YES];
    }
    
    
}

-(void)imagePickerController: (UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType =
    [info objectForKey:@"UIImagePickerControllerMediaType"];
    //[[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [self.navigationController setToolbarHidden:NO animated:YES];
    selGoogle=NO;
    [self._selGoogle setOn:selGoogle];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage])
    {
        UIImage * edited_image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        UIImage *original_image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        if (edited_image!=nil) image_post=edited_image; else image_post=original_image;
    }
    NSURL *url = [info objectForKey:UIImagePickerControllerReferenceURL];
    //NSLog(@" Info %@",info);
    image_ref= [url relativeString];
    
    [self dismissModalViewControllerAnimated:YES];
    [popoverController dismissPopoverAnimated:YES];
    
    self._postImage.image=image_post;
    self._back_postimage.hidden=NO;
    self._postImage.hidden=NO;
    self._label_uso.hidden=YES;
    self._pickImage.hidden=YES;
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    // [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
    [self dismissModalViewControllerAnimated:YES];
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    // [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];
       [self.navigationController setToolbarHidden:NO animated:YES];
    
}


- (void)viewDidUnload
{
    [self set_postText:nil];
    [self set_flyButton:nil];
    [self set_profImage:nil];
    [self set_postImage:nil];
    [self set_selFacebook:nil];
    [self set_selTwitter:nil];
    [self set_selLinkedin:nil];
    [self set_selGoogle:nil];
    [self set_labelConter:nil];
    [self set_labelProfName:nil];
    [self set_backSky:nil];
    [self set_spriteBallon:nil];
    [self set_labelFlyMessage:nil];
    [self set_back_profimage:nil];
    [self set_back_postimage:nil];
    [self set_label_face:nil];
    [self set_label_twitter:nil];
    [self set_label_linkedin:nil];
    [self set_label_google:nil];
    [self set_pickImage:nil];
    [self set_getStore:nil];
    [self set_labelFreeLimit:nil];
    [self set_label_uso:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
