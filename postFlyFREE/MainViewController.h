//
//  MainViewController.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MainViewController.h"
#import "SocialBinder.h"
#import "TFTTapForTap.h"

@interface MainViewController : UIViewController <BinderDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UIActionSheetDelegate,UITextViewDelegate,UIActionSheetDelegate,UIAlertViewDelegate,TFTBannerDelegate> {
    
    @private
            BOOL selFacebook;
            BOOL selTwitter;
            BOOL selLinkedin;
            BOOL selGoogle;
            UIImage * image_post;
            NSString * image_ref;
            BOOL first_time;
            BOOL facebook_with_image;
            NSString * temp_message;
            int finish_conter;
            BOOL isFinished;
            BOOL againCheckin;
            BOOL isChecked;
            // alert rate
            BOOL alert_rate;
    
}

@property (strong, nonatomic) SocialBinder *socialB;

@property (strong, nonatomic) IBOutlet UIImageView *_profImage;
@property (strong, nonatomic) IBOutlet UIImageView *_back_profimage;
@property (strong, nonatomic) IBOutlet UIImageView *_postImage;
@property (strong, nonatomic) IBOutlet UIImageView *_back_postimage;
@property (strong, nonatomic) IBOutlet UILabel *_labelConter;
@property (strong, nonatomic) IBOutlet UILabel *_labelProfName;
@property (strong, nonatomic) IBOutlet UILabel *_label_face;
@property (strong, nonatomic) IBOutlet UILabel *_label_twitter;
@property (strong, nonatomic) IBOutlet UILabel *_label_linkedin;
@property (strong, nonatomic) IBOutlet UILabel *_label_google;
@property (strong, nonatomic) IBOutlet UILabel *_labelFreeLimit;
@property (strong, nonatomic) IBOutlet UILabel *_label_uso;

@property (strong, nonatomic) IBOutlet UITextView *_postText;

@property (strong, nonatomic) IBOutlet UIButton *_flyButton;
@property (strong, nonatomic) IBOutlet UIButton *_pickImage;
@property (strong, nonatomic) IBOutlet UIButton *_getStore;

@property (strong, nonatomic) IBOutlet UISwitch *_selFacebook;
@property (strong, nonatomic) IBOutlet UISwitch *_selTwitter;
@property (strong, nonatomic) IBOutlet UISwitch *_selLinkedin;
@property (strong, nonatomic) IBOutlet UISwitch *_selGoogle;

@property (strong, nonatomic) UIPopoverController *popoverController;

@property (strong, nonatomic) IBOutlet UIImageView *_backSky;

@property (strong, nonatomic) IBOutlet UIImageView *_spriteBallon;
@property (strong, nonatomic) IBOutlet UILabel *_labelFlyMessage;


-(IBAction)selectSocial:(id)select;
-(IBAction)camera:(id)sender;
-(IBAction)pickImage:(id)sender;
-(IBAction)sendPostFly:(id)sender;
-(void)callConfig;
-(void)callLog;
-(void)fechar;
-(IBAction)clearContent:(id)sender;
-(IBAction)getFull:(id)sender;
-(void)setAgainCheckinl:(BOOL)flag;

@end
