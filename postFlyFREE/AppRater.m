//
//  AppRater.m
//  postFlyFREE
//
//  Created by Felipe Work on 23/02/13.
//  Copyright (c) 2013 Felipe Menezes. All rights reserved.
//

#import "AppRater.h"

@implementation AppRater

#define APP_RATE_USE  2 
#define APP_ID @"574020759"


+(void)setRateMessage:(NSString *)message {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:message forKey:@"rate_message"];
    [defaults synchronize];
}

+(void)setRated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:@"rate_ok"];
    [defaults synchronize];
}

+(BOOL)getRated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [[defaults valueForKey:@"rate_ok"] boolValue];
}

+(NSString *)getRateMessage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults valueForKey:@"rate_message"];
}

+(BOOL)conterUsage {
    if ([self getRated]) return NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int days = [[defaults valueForKey:@"rate_user_cont"] intValue];
    days++;
    if (days>APP_RATE_USE) {
        days=0;
        [defaults setValue:[NSNumber numberWithInt:days] forKey:@"rate_user_cont"];
        [defaults synchronize];
        return YES;
    }
    [defaults setValue:[NSNumber numberWithInt:days] forKey:@"rate_user_cont"];
    [defaults synchronize];
    return NO;
}

+(void)callReview {
    NSURL *appStoreURL = [NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%@",APP_ID]];
    [[UIApplication sharedApplication] openURL:appStoreURL];
    [self setRated];
}

@end
