//
//  FacebookBinder.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 03/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Binder.h"
#import <FacebookSDK/FacebookSDK.h>

@interface FacebookBinder : Binder
@property (strong, nonatomic) UISwitch *temp_switch;

- (id)init;
-(BOOL) reauthorizeForPublish;

@end
