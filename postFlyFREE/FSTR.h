//
//  Fstr.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 05/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FSTR : NSObject

+(NSString *)fstr :(NSString *) key;

@end
