//
//  AppDelegate.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 30/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialBinder.h"
#import "MainViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,BinderDelegate>



extern NSString *const FBSessionStateChangedNotification;



@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) UINavigationController *_navigationController;

@property (strong, nonatomic) SocialBinder *socialB;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
