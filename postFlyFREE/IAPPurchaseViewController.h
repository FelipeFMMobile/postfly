//
//  IAPPPurchaseViewController.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/11/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface IAPPurchaseViewController : UIViewController <SKProductsRequestDelegate, SKPaymentTransactionObserver, UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UINavigationBar *_navbar;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *_indicator;
@property (strong, nonatomic) IBOutlet UILabel *_label_info;
+(BOOL)IAPItemPurchased;

-(IBAction)confirm_purchase :(id)sender;
-(IBAction)sair :(id)sender;
+(void)deleteKeyChain :(id)sender;
@end
