//
//  SocialBinder.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "SocialBinder.h"
#import "Reachability.h"
#import "FSTR.h"
#import "ItePost.h"
#import "AppDelegate.h"

@implementation SocialBinder

@synthesize parentView;
@synthesize facebook;
@synthesize twitter;
@synthesize linkedin;
@synthesize googleplus;
@synthesize image_ref_url;
@synthesize temp_message;

- (id)init
{
    if (self = [super init]) {
        self.facebook = [[FacebookBinder alloc] init];
        self.googleplus = [[GoogleBinder alloc] init];
        self.twitter= [[TwitterBinder alloc] init];
        self.linkedin= [[LinkedinBinder alloc] init];
        
        AppDelegate * appDel =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        self.managedObjectContext =appDel.managedObjectContext;
        
        return self;
        
    }
    
    
    return nil;
}

-(void)setDelegateForBinders:(id)delegate {
    self.facebook.delegate=delegate;
    self.googleplus.delegate=delegate;
    self.twitter.delegate=delegate;
    self.linkedin.delegate=delegate;
    parentView=delegate;
}

-(void)setUseFor:(NSString *) net :(BOOL) flag :(UIViewController *) view {
    if ([net isEqualToString:@"facebook"]) {
        [self.facebook setUse:flag];
    }
    if ([net isEqualToString:@"twitter"]) {
        [self.twitter setUse:flag];
    }
    if ([net isEqualToString:@"linkedin"]) {
        [self.linkedin setViewLogin:view];
        [self.linkedin setUse:flag];
    }
    if ([net isEqualToString:@"googleplus"]) {
        [self.googleplus setUse:flag];
    }
    

}

-(void)checkSign {
    


    
    #pragma  FACEBOOK TESTE
    
    if ([self.facebook isUse]) {
        if ([self.facebook checkSigned]) {
           // NSLog(@"Voce esta usando o Facebook");
        }
    }
    
#pragma  GOOGLE TESTE
    
    if ([self.googleplus isUse]) {
        if ([self.googleplus checkSigned]) {
            // NSLog(@"Voce esta usando o GooglePlus");
        }
    }
    
    #pragma  TWITTER TESTE
    
    if ([self.twitter isUse]) {
        //NSLog(@"Voce esta usando o Twitter");
        if ([self.twitter checkSigned]) {
        }
    }
     #pragma  LINKEDIN TESTE
   
    if ([self.linkedin isUse]) {
       // NSLog(@"Voce esta usando o Linkedin");
        if (parentView!=nil) [self.linkedin setViewLogin:parentView];
        if ([self.linkedin checkSigned]) {
        }
    }
    
}

-(BOOL)postVerify:(NSString *)texto {
    
    int total = [texto length];
    if (total>140) flag_twitter_pass=NO;  else flag_twitter_pass=YES;
    return flag_twitter_pass;
        
}

-(NSString *)callShortner:(NSString *)texto {
    
    NSString *testString = texto;
    NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes :NSTextCheckingTypeLink error:nil];
    NSArray *matches = [detect matchesInString:testString options:0 range:NSMakeRange(0, [testString length])];
    NSURL *url=nil;
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            url= [match URL];
        }
    }
    if (url!=nil ) {
        
        NSString *longURL = [url relativeString];
        if ([@"http://goo.gl" rangeOfString:longURL].location != NSNotFound) return texto;
        
        //NSLog(@"%@", longURL);
        NSData *reqData = [[NSString stringWithFormat:@"{\"longUrl\":\"%@\"}", longURL] dataUsingEncoding:NSUTF8StringEncoding];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"https://www.googleapis.com/urlshortener/v1/url"]
                                        cachePolicy:NSURLRequestUseProtocolCachePolicy
                                        timeoutInterval:20];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:reqData];
        
        NSError *err = [[NSError alloc] init];
        NSData *retData = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:nil
                                                            error:&err];
        
        if([err domain]) return nil;
        
        NSString *retString = [[NSString alloc] initWithData:retData encoding:NSUTF8StringEncoding];
           
        // deserializando encurtador
        NSError *jsonError;
        NSArray *shortner =
        [NSJSONSerialization
         JSONObjectWithData:retData
         options:NSJSONReadingMutableLeaves
         error:&jsonError];
        if (shortner) {
            // at this point, we have an object that we can parse
           // NSLog(@"URL SHORTNED %@", [shortner valueForKey:@"id"]);
            recent_url_shortened=[shortner valueForKey:@"id"];
            if (recent_url_shortened!=nil)
                return [testString stringByReplacingOccurrencesOfString:longURL withString:recent_url_shortened];
        }
        
        if([retString rangeOfString:@"\"error\""].length) {
           
        }
    }
    return texto;
}
-(NSString *)getRecentUrlShortned {
    return recent_url_shortened;
}
- (BOOL)isConnected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_con"]
                                                        message:[FSTR fstr:@"alert_mens_con"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

-(BOOL)postFly :(NSString *) caption :(NSString *) message :(NSString *) link :(NSArray *) redes :(UIImage *)image{
    if ([self isConnected]) {
        
        BOOL is_facebook_image=NO;
        self.temp_message=message;
        
        BOOL selected_social_not_signed=NO;
        
        // loop que verifica se estao todas sinalizadas
        for (NSString * rede in redes) {
            if ([rede isEqualToString:@"facebook"]) {
                if ([self.facebook isUse]) {
                    if  (![self.facebook isSigned]) {
                        selected_social_not_signed=YES;
                    }
                }
            }
            if ([rede isEqualToString:@"twitter"]) {
                if ([self.twitter isUse]) {
                    if  (![self.twitter isSigned]) {
                        selected_social_not_signed=YES;
                    }
                }
            }
            if ([rede isEqualToString:@"linkedin"]) {
                if ([self.linkedin isUse]) {
                    if  (![self.linkedin isSigned]) {
                        selected_social_not_signed=YES;
                    }
                }
            }
        } // for
        
        // verifica resultado
          if (selected_social_not_signed) {
              return NO;
          }
        
        for (NSString * rede in redes) {
           
            if ([rede isEqualToString:@"facebook"]) {
                if ([self.facebook isUse]) {
                    if  ([self.facebook isSigned]) {
                        if (image!=nil) {
                            is_facebook_image=YES;
                                [self.facebook postImage:caption :message :image :link];
                            } else {
                                [self.facebook postWall:caption :message :link];
                            }
                    } else selected_social_not_signed=YES;
                }
            }
            
            if ([rede isEqualToString:@"twitter"]) {
                if ([self.twitter isUse]) {
                    if  ([self.twitter isSigned]) {
                        NSString * m = message;
                        if (!flag_twitter_pass) m=[m substringFromIndex:140];
                        if (image!=nil) {
                                [self.twitter postImage:caption :m   :image :link];
                        } else {
                            [self.twitter postWall:caption :m  :link];
                        }
                    } else selected_social_not_signed=YES;
                }
            }
            
            if ([rede isEqualToString:@"linkedin"]) {
                // corta o post para o linkedin se tiver imagem e for para o facebook
                if (!is_facebook_image) {
                    if ([self.linkedin isUse]) {
                        if  ([self.linkedin isSigned]) {
                            [self.linkedin postWall:caption :message :link];
                        } else selected_social_not_signed=YES;
                    }
                }
            }
              
        } // for
        
        if ([redes count]>0) {
            
            // salvando dados no arquivo.
            if (!selected_social_not_signed) {
                [self savePost:caption:message:redes : image];
            } else {
                return NO;
            }
            
            return YES;
        }
        
        
    }
    
    
    return NO;
}

-(BOOL)savePost :(NSString *) caption :(NSString *) message :(NSArray *) redes :(UIImage *)image{
    PostInfo * post_table= [NSEntityDescription insertNewObjectForEntityForName:@"PostInfo"
                                                 inManagedObjectContext:self.managedObjectContext];

    post_table.caption=caption;
    post_table.message=message;
    post_table.data=[NSDate date];
    
  //  post_table.image_ref= image
    
    for (NSString * rede  in redes) {
        ItePost * item= [NSEntityDescription insertNewObjectForEntityForName:@"ItePost"
                                                             inManagedObjectContext:self.managedObjectContext];
        item.rede_nome=rede;
        item.postinfo=post_table;
        
    }
    if (image_ref_url!=nil) 
    post_table.image_ref=image_ref_url;
    
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Error %@",error);
    } else {
        recent_post=post_table;
        return YES;
    }
   
    return NO;
}
-(void)setRecentPost:(NSManagedObject *)post{
    recent_post=post;
}
-(NSManagedObject *)getRecentPost {
    return recent_post;
}

-(NSMutableArray *)queryPosts {
    
    // carregando o dados
    // FETCH (SELECT) THE DATA TO RETURN STORED EVENTS.
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PostInfo" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    // DEFININDO ORDENACAO
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"data" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
 
    // EXECUTANDO
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }
    // PASSANDO O RESULTADO PARA A EVENTS ARRAY PRINCIPAL
    return mutableFetchResults;

    
}

@end
