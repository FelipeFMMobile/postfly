//
//  AppRater.h
//  postFlyFREE
//
//  Created by Felipe Work on 23/02/13.
//  Copyright (c) 2013 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppRater : NSObject

+(void)setRateMessage:(NSString *)message;
+(void)setRated;
+(BOOL)getRated;
+(NSString *)getRateMessage;
+(BOOL)conterUsage;
+(void)callReview;

@end
