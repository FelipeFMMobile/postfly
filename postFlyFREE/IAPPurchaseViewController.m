//
//  IAPPPurchaseViewController.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/11/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "IAPPurchaseViewController.h"
#import  "SFHFKeychainUtils.h"
#import "FSTR.h"

@interface IAPPurchaseViewController ()

@end

#define kStoredData @"com.fmmobile.postFlyFULL"

@implementation IAPPurchaseViewController

@synthesize _label_info,_indicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super viewDidLoad];
       
    self._navbar.topItem.title=[FSTR fstr:@"title_purchase"];

}

// function to be called after call th app
+(BOOL)IAPItemPurchased {
    
    // check userdefaults key
    
    NSError *error = nil;
    NSString *password = [SFHFKeychainUtils getPasswordForUsername:@"postuser" andServiceName:kStoredData error:&error];
    
    if ([password isEqualToString:@"postfly"]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults  setBool:YES forKey:@"app_purchase"];
        return YES;
    }
    else {
       return NO; 
    }
   
   
}

+(void)deleteKeyChain:(id)sender {
    
    NSError *error = nil;
    
    //[SFHFKeychainUtils deleteItemForUsername:@"<span class='Apple-style-span' style='font-family: Times; font-size: small;'>postuser</span>" andServiceName:kStoredData error:&error];
    [SFHFKeychainUtils deleteItemForUsername:@"postuser" andServiceName:kStoredData error:&error];

}

-(IBAction)confirm_purchase:(id)sender {
    if ([SKPaymentQueue canMakePayments]) {
        
        SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:@"com.fmmobile.postFlyFULL"]];
        
        request.delegate = self;
        [request start];
        _label_info.hidden=NO;
        _label_info.text=[FSTR fstr:@"iapp_status_waiting"];
        _indicator.hidden=NO;
        
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:[FSTR fstr:@"iapp_title_prohibited"]
                            message:[FSTR fstr:@"iapp_title_prohibited_message"]
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];

    }

}

-(IBAction)sair:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}
#pragma mark StoreKit Delegate

// CONFIRM PURCHASE
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    
    // remove wait view here
    _label_info.hidden=YES;
    _indicator.hidden=YES;
    
    SKProduct *validProduct = nil;
    int count = [response.products count];
    
    if (count>0) {
        validProduct = [response.products objectAtIndex:0];
        
        SKPayment *payment = [SKPayment paymentWithProduct:validProduct];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
        
    } else {
        UIAlertView *tmp = [[UIAlertView alloc]
                            initWithTitle:@"Not Available"
                            message:@"No products to purchase"
                            delegate:self
                            cancelButtonTitle:nil
                            otherButtonTitles:@"Ok", nil];
        [tmp show];
        
    }
    
    
}


// RESULT OF TRANSACTION 
-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    
    
    for (SKPaymentTransaction *transaction in transactions) {
       
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing: {
                
                // show wait view here
                _label_info.text = [FSTR fstr:@"iapp_status_process"];
                _label_info.hidden=NO;
                _indicator.hidden=NO;
                break;
            }
            case SKPaymentTransactionStatePurchased: {
                
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                // remove wait view and unlock feature 2
                _label_info.text = [FSTR fstr:@"iapp_status_done"];
                 _indicator.hidden=YES;
                UIAlertView *tmp = [[UIAlertView alloc]
                                    initWithTitle:[FSTR fstr:@"iapp_title_complete"]
                                    message:[FSTR fstr:@"iapp_title_complete_message"]
                                    delegate:self
                                    cancelButtonTitle:nil
                                    otherButtonTitles:@"Ok", nil];
                          
                
                NSError *error = nil;
                [SFHFKeychainUtils storeUsername:@"postuser" andPassword:@"postfly" forServiceName:kStoredData updateExisting:YES error:&error];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults  setBool:YES forKey:@"app_purchase"];
                [self sair:nil];
                [tmp show];
                
                break;
            }
            case SKPaymentTransactionStateRestored: {
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                // remove wait view here
                _label_info.hidden=YES;
                _indicator.hidden=YES;

                break;
            }
            case SKPaymentTransactionStateFailed: {
                
                if (transaction.error.code != SKErrorPaymentCancelled) {
                    NSLog(@"Error payment cancelled %@",transaction.error.description);
                }
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                // remove wait view here
                _label_info.text=[FSTR fstr:@"iapp_status_cancelled"];
                _indicator.hidden=YES;
                break;
            }
            default:
                break;
        }
    } // for
}


-(void)requestDidFinish:(SKRequest *)request
{
   
}

-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"Failed to connect with error: %@", [error localizedDescription]);
}  




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self set_label_info:nil];
    [self set_indicator:nil];
    [self set_navbar:nil];
    [super viewDidUnload];
}
@end
