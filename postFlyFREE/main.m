//
//  main.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 30/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
