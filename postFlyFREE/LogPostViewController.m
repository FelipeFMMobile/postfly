//
//  LogPostViewController.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 11/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "LogPostViewController.h"
#import "FSTR.h"
#import "LogViewController.h"

@interface LogPostViewController ()

@end

@implementation LogPostViewController
@synthesize socialB,posts;
@synthesize customCell1;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.toolbarHidden=YES;
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = YES;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    posts=[socialB queryPosts];
    
    self.title=[FSTR fstr:@"title_list"];
    
      //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back4.png"]];
     
    
}
- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [posts count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [FSTR fstr:@"title_list"];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
           if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                   [[NSBundle mainBundle] loadNibNamed:@"LogPostCell-iPad" owner:self options:nil];
           } else {
                   [[NSBundle mainBundle] loadNibNamed:@"LogPostCell" owner:self options:nil];
           }
    
        cell = customCell1;
        self.customCell1 = nil;
    }
    UILabel *label_post;
    label_post = (UILabel *)[cell viewWithTag:1];

    NSManagedObject * data=[posts objectAtIndex:indexPath.row];
    
    label_post.text=[NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
    
    NSDateFormatter *dateFormatter = nil;
    NSString * tdata;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSString *identifier = [[NSLocale currentLocale] localeIdentifier];
       // NSLog(@"Localizacao %@",identifier);
        if  ([@"pt_BR" rangeOfString:identifier].location != NSNotFound )   [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
        else  [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm"];
        // [dateFormatter setTimeStyle:NSTimeZoneNameStyleShortGeneric];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        tdata=[dateFormatter stringFromDate:[data valueForKey:@"data"]];
    }
     UILabel *label_data;
    label_data = (UILabel *)[cell viewWithTag:6];
    label_data.text=[NSString stringWithFormat:@"%@",tdata];
    
    
    NSArray * itens = [[data valueForKey:@"itepost"] allObjects];
    
    UIImageView* faceImg = (UIImageView *)[cell viewWithTag:2];
    UIImageView * twitImg = (UIImageView *)[cell viewWithTag:3];
    UIImageView * linkImg = (UIImageView *)[cell viewWithTag:4];
    UIImageView * gooImg = (UIImageView *)[cell viewWithTag:5];
    
    for (NSManagedObject * item in itens) {
        NSString * rede = [item valueForKey:@"rede_nome"];
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
            if ([item valueForKey:@"flag_sucess"]!=nil) {
               if ([[item valueForKey:@"flag_sucess"] boolValue])
                  faceImg.image=[UIImage imageNamed:@"fb_b1"];
               else
                  faceImg.image=[UIImage imageNamed:@"fb_b3"];
            } else {
                faceImg.image=[UIImage imageNamed:@"fb_b4"];
            }
        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            if ([item valueForKey:@"flag_sucess"]!=nil) {
               if ([[item valueForKey:@"flag_sucess"] boolValue]) 
                    twitImg.image=[UIImage imageNamed:@"tw_b1"];
                else
                    twitImg.image=[UIImage imageNamed:@"tw_b3"];
            } else {
                twitImg.image=[UIImage imageNamed:@"tw_b4"];
            }
        }
        if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
             if ([item valueForKey:@"flag_sucess"]!=nil) {
                 if ([[item valueForKey:@"flag_sucess"] boolValue])
                   linkImg.image=[UIImage imageNamed:@"lk_b1"];
                 else linkImg.image=[UIImage imageNamed:@"lk_b3"];
             } else {
                  linkImg.image=[UIImage imageNamed:@"lk_b4"];
             }
        }
        if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
            if ([item valueForKey:@"flag_sucess"]!=nil) {
                if ([[item valueForKey:@"flag_sucess"] boolValue])
                    gooImg.image=[UIImage imageNamed:@"gp_b1"];
                else gooImg.image=[UIImage imageNamed:@"gp_b3"];
            } else {
                 gooImg.image=[UIImage imageNamed:@"gp_b4"];
            }
        }
    }
    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject * data=[posts objectAtIndex:indexPath.row];
    NSArray * itens = [[data valueForKey:@"itepost"] allObjects];
    
    LogViewController *logViewController=nil;
      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
          logViewController = [[LogViewController alloc] initWithNibName:@"LogView-iPad" bundle:nil];
      } else {
              logViewController = [[LogViewController alloc] initWithNibName:@"LogView" bundle:nil];
      }
    logViewController.message=[NSString stringWithFormat:@"%@",[data valueForKey:@"message"]];
    logViewController.data=[data valueForKey:@"data"];
    logViewController.post=data;
    logViewController.imageref=[data valueForKey:@"image_ref"];
    logViewController.socialB=self.socialB;
    logViewController.itemPost=itens;
    
    [self.navigationController pushViewController:logViewController animated:YES];
    
}

@end
