//
//  WebViewController.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 16/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *_webview;
@property (strong, nonatomic) IBOutlet UIButton *_btVoltar;
@property (strong, nonatomic) IBOutlet UINavigationBar *_navbar;
@property (strong, nonatomic) IBOutlet UINavigationItem *_navitem;
@property (strong, nonatomic) IBOutlet UILabel *_label_wait;

-(IBAction)sair:(id)sender;

@end
