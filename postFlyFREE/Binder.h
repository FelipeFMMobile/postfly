//
//  Binder.h
//  postFlyFree
//
//  Created by Felipe Menezes on 28/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol BinderDelegate <NSObject>

    
    @optional
    - (void)didLogin:(BOOL )sucess :(NSString *)socialID;

    - (void)didPost:(BOOL) sucess :(NSString *)socialID;

    - (void)didPostImageWithUrl:(BOOL) sucess :(NSString *)url;
    
    - (void)didGetProfile:(BOOL) sucess :(NSString *)socialID;

@end

@interface Binder : NSObject {
    
    
    __weak id <BinderDelegate>delegate;


@private
    BOOL is_Use;
    BOOL is_Signed;
    NSString * socialId;
    NSString * token;
    NSString * app_Id;
    NSString * app_Secret;
    NSString * url_String_Id;
    NSData * img_data;
}

@property(nonatomic, weak) id <BinderDelegate>delegate;

@property(retain,nonatomic) NSString * profile_name;
@property(retain,nonatomic) NSString * profile_info;
@property(retain,nonatomic) NSString * profile_id;
@property(retain,nonatomic) NSString * profile_pic;
@property(retain,nonatomic) NSString * profile_wall_url;
@property(retain,nonatomic) UIImage * profile_image;

-(id)init;
-(id)initSocialId:(NSString *)social;

-(BOOL)isConnected;

-(BOOL)login;
-(BOOL)logout;
-(BOOL)checkSigned;
-(BOOL)postWall:(NSString *) caption :(NSString *) message :(NSString *) link;
-(BOOL)postImage:(NSString *) caption :(NSString *) message :(UIImage *)image :(NSString *) link;


-(BOOL)isUse;
-(BOOL)isSigned;

-(void) setUse: (BOOL) use;
-(void) setSigned: (BOOL) sign;
-(void) setToken: (NSString *) t;
-(void) setAppId: (NSString *) app;

-(void)getProfileInfo;
-(void)setUrlString:(NSString*) url_id;
-(NSString *)getUrlString;
-(BOOL)checkUrlString:(NSString*) url;
- (UIImage*)imageByScalingAndCroppingForSize:(UIImage *) sourceImage :(float )percent_reduction;
- (UIImage*)imageByScalingAndCroppingForSize:(UIImage *) sourceImage :(float )w :(float)h;

@end
