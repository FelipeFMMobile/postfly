//
//  LogWebViewController.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 12/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "LogViewController.h"
#import "WebViewController.h"
#import "FSTR.h"
#import "IAPPurchaseViewController.h"

@interface LogViewController (PrivateMethods)


-(void)findLargeImage:(NSString *)path;

@end

@implementation LogViewController
@synthesize _labelPost;
@synthesize _btFacebook;
@synthesize _btTwitter;
@synthesize _btLinkedin;
@synthesize _btGoogle;
@synthesize _labelData;
@synthesize _getStore;

@synthesize itemPost;
@synthesize post;
@synthesize _imagepost;
@synthesize imageref;
@synthesize _bt_resend;
@synthesize _back_image_post;
@synthesize message;
@synthesize data;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        
     self.title=[FSTR fstr:@"title_postinfo"];
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:@"app_purchase"]) {
        _getStore.hidden=YES;
    } else {
        _getStore.hidden=NO;
    }
       
    if (self.imageref!=nil) {
        self._imagepost.hidden=NO;
        self._back_image_post.hidden=NO;
        [self findLargeImage:self.imageref];
    }
        
    [self setButtons];
    
    self._labelPost.text=message;
    
    NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSString *identifier = [[NSLocale currentLocale] localeIdentifier];
        if  ([@"pt_BR" rangeOfString:identifier].location != NSNotFound ) [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
        else  [dateFormatter setDateFormat:@"MM-dd-yyyy HH:mm"];

        // [dateFormatter setTimeStyle:NSTimeZoneNameStyleShortGeneric];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        self._labelData.text=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:self.data]];
    }

        

           
    
}

-(void)setButtons {
    for (NSManagedObject * item in self.itemPost) {
        NSString * rede = [item valueForKey:@"rede_nome"];
       
        
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
            selFacebook=YES;
            [self._btFacebook setSelected:selFacebook];
              if ([item valueForKey:@"flag_sucess"]!=nil) {
                  if (![[item valueForKey:@"flag_sucess"] boolValue])
                      [self._btFacebook setImage:[UIImage imageNamed:@"fb_b3"] forState:UIControlStateSelected];
                  else
                      [self._btFacebook setImage:[UIImage imageNamed:@"fb_b1"] forState:UIControlStateSelected];
              } else {
                  [self._btFacebook setImage:[UIImage imageNamed:@"fb_b4"] forState:UIControlStateSelected];
              }
        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            selTwitter=YES;
            [self._btTwitter setSelected:selTwitter];
            if ([item valueForKey:@"flag_sucess"]!=nil) {
                if (![[item valueForKey:@"flag_sucess"] boolValue])
                    [self._btTwitter setImage:[UIImage imageNamed:@"tw_b3"] forState:UIControlStateSelected];
                else
                    [self._btTwitter setImage:[UIImage imageNamed:@"tw_b1"] forState:UIControlStateSelected];
            } else {
                [self._btTwitter setImage:[UIImage imageNamed:@"tw_b4"] forState:UIControlStateSelected];
            }
        }
        if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
            selLinkedin=YES;
            [self._btLinkedin setSelected:selLinkedin];
            if ([item valueForKey:@"flag_sucess"]!=nil) {
                if (![[item valueForKey:@"flag_sucess"] boolValue])
                    [self._btLinkedin setImage:[UIImage imageNamed:@"lk_b3"] forState:UIControlStateSelected];
                else
                    [self._btLinkedin setImage:[UIImage imageNamed:@"lk_b1"] forState:UIControlStateSelected];
            } else {
                 [self._btLinkedin setImage:[UIImage imageNamed:@"lk_b4"] forState:UIControlStateSelected];
            }
        }
        if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
            selGoogle=YES;
            [self._btGoogle setSelected:selGoogle];
            if ([item valueForKey:@"flag_sucess"]!=nil) {
                if (![[item valueForKey:@"flag_sucess"] boolValue])
                    [self._btGoogle setImage:[UIImage imageNamed:@"gp_b3"] forState:UIControlStateSelected];
                else
                   [self._btGoogle setImage:[UIImage imageNamed:@"gp_b1"] forState:UIControlStateSelected];
            } else {
                [self._btGoogle setImage:[UIImage imageNamed:@"gp_b4"] forState:UIControlStateSelected];
            }
        }
    } // for

}

// find Image
-(void)findLargeImage:(NSString *)path;
{

    //
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *rep = [myasset defaultRepresentation];
        CGImageRef iref = [rep fullResolutionImage];
        if (iref) {
           self._imagepost.image=[UIImage imageWithCGImage:iref];
        }
    };
    
    //
    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        //NSLog(@"Cant get image - %@",[myerror localizedDescription]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_error_asset"]
                                                        message:[FSTR fstr:@"alert_mens_error_asset"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    };
    
         NSURL *asseturl = [NSURL URLWithString:path];
        ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];

        [assetslibrary assetForURL:asseturl
                       resultBlock:resultblock
                      failureBlock:failureblock];
}


-(IBAction)openLink:(id)select {
    WebViewController * web = [[WebViewController alloc] init];
    web.modalTransitionStyle = UIModalTransitionStyleCoverVertical;

    if (select==self._btFacebook) {
        if (selFacebook) {
            NSString * wall=[NSString stringWithFormat:@"%@",self.socialB.facebook.profile_wall_url];
            if ([self.socialB isConnected]) {
                //NSURL *url = [NSURL URLWithString:wall];
                  // [[UIApplication sharedApplication] openURL:url];
            }
            [self presentModalViewController:web animated:YES];
            [ web._webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:wall]]];
        }
        [self._btFacebook setSelected:selFacebook];
    }
    if (select==self._btTwitter) {
        if (selTwitter) {
            NSString * wall=[NSString stringWithFormat:@"http://www.twitter.com/%@",self.socialB.twitter.profile_name];
            if (self.socialB.isConnected) {
                [self presentModalViewController:web animated:YES];
                [ web._webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:wall]]];
           
            }
        }
        [self._btTwitter setSelected:selTwitter];
    }
    if (select==self._btLinkedin) {
        if (selLinkedin) {
            NSString * wall=[NSString stringWithFormat:@"%@",self.socialB.linkedin.profile_wall_url];
            if (self.socialB.isConnected) {
                [self presentModalViewController:web animated:YES];
                [ web._webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:wall]]];
            }
    }
        [self._btLinkedin setSelected:selLinkedin];
    }
    if (select==self._btGoogle) {
        if (selGoogle) {
            NSString * wall=@"http://plus.google.com";
            if (self.socialB.isConnected) {
                [self presentModalViewController:web animated:YES];
                [ web._webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:wall]]];
           }
                
        }
        [self._btGoogle setSelected:selGoogle];
    }
    
}


-(IBAction)getFull:(id)sender {
  /*  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/us/app/postfly-for-facebook-twitter/id574022761?ls=1&mt=8"]];*/
    IAPPurchaseViewController * iappView = [[IAPPurchaseViewController alloc]  initWithNibName:@"IAPPurchaseView" bundle:nil];
     iappView.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentModalViewController:iappView animated:YES];
    


}

// com este implementação permite ver o resultado do delegate se ainda estiver tranmitindo o original.
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
  //  [self.socialB setDelegateForBinders:self];
}

-(IBAction)reSend:(id)sender{
    
    if (![self.socialB isConnected])
            return;
    
    [self.socialB setDelegateForBinders:self];
    
    is_facebook_image=NO;
    UIImage * image = self._imagepost.image;

    
    for (NSManagedObject * item in self.itemPost) {
        NSString * rede = [item valueForKey:@"rede_nome"];
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
            if (![[item valueForKey:@"flag_sucess"] boolValue]) {
                if (image!=nil) {
                    is_facebook_image=YES;
                    [self.socialB.facebook postImage:message :message :image :nil];
                } else {
                    [self.socialB.facebook postWall:message :message :nil];
                }
            }

        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            if (![[item valueForKey:@"flag_sucess"] boolValue]) {
                if (image!=nil) {
                    [self.socialB.twitter postImage:message :message :image :nil];
                } else {
                    [self.socialB.twitter postWall:message :message :nil];
                }
            }
        }
        if (!is_facebook_image) {
            if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
                if (![[item valueForKey:@"flag_sucess"] boolValue]) {
                       [self.socialB.linkedin postWall:message :message :nil];
                }
            }
            if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
                if (![[item valueForKey:@"flag_sucess"] boolValue]) {
                     [self.socialB.googleplus postWall:message :message :nil];
                }
            }
        }
    } // for
    [self.socialB setRecentPost:self.post];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_resend"]
                                                    message:[FSTR fstr:@"alert_mens_resend"]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];

    
}
#pragma delegate metodos

- (void)didPostImageWithUrl :(BOOL)sucess :(NSString *)url {
        // completa a acao para outras midias sociais, que nao tem como fazer upload de foto.
        [self.socialB.facebook postWall:self.socialB.temp_message :self.socialB.temp_message :url];  
        for (NSManagedObject * item in self.itemPost) {
            
            NSString * rede = [item valueForKey:@"rede_nome"];
            
            if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
                if (is_facebook_image) {
                    [self.socialB.linkedin postWall:nil:self.message :url];
                }
            }
            
        } 
}

-(void)didPost:(BOOL)sucess :(NSString *)socialID {
    
     for (NSManagedObject * item in self.itemPost) {
        
        NSString * rede = [item valueForKey:@"rede_nome"];
        
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
            if ([@"facebook" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
           }
        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            if ([@"twitter" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
           }
        }
        if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
            if ([@"linkedin" rangeOfString:socialID].location != NSNotFound) {
                    [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
            }
        }
        if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
            if ([@"googleplus" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
             }
        }
    }
  NSError *error = nil;
    if (![self.socialB.managedObjectContext save:&error]) {
        //NSLog(@"Error %@",error);
    }
    
    [self setButtons];
    
   // NSLog(@"Post em %@ realizado %i",socialID,sucess);
}


- (void)viewDidUnload
{
    [self set_labelPost:nil];
    [self set_btFacebook:nil];
    [self set_btTwitter:nil];
    [self set_btLinkedin:nil];
    [self set_btGoogle:nil];
    [self set_labelData:nil];
    [self set_getStore:nil];
    [self set_imagepost:nil];
    [self set_back_image_post:nil];
    [self set_bt_resend:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
