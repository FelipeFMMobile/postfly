//
//  Binder.m
//  postFlyFree
//
//  Created by Felipe Menezes on 28/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "Binder.h"
#import "Reachability.h"
#import "FSTR.h"

@implementation Binder

@synthesize  profile_name;
@synthesize  profile_info;
@synthesize  profile_id;
@synthesize  profile_pic;
@synthesize  profile_wall_url;
@synthesize  profile_image;
@synthesize delegate;

- (id)init
{
    // avoid initialize with this method
    [self doesNotRecognizeSelector:_cmd];
    return nil;
}
-(id)initSocialId:(NSString *)social{
    self = [super init];
    if (self) {
        delegate = nil;
        socialId=social;
        [self isUse];
    }
    return self;
}

- (BOOL)isConnected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_title_con"]
                                                        message:[FSTR fstr:@"alert_mens_con"]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

-(BOOL)login{
    return [self isConnected];

};

-(BOOL)logout{
    [self setSigned:NO];
    return YES;
};

-(BOOL)postWall:(NSString *) caption :(NSString *) message :(NSString *) link{
    return [self isConnected];
}
-(BOOL)postImage:(NSString *) caption :(NSString *) message :(UIImage *)image :(NSString *) link{
    if (image.size.width>612 && image.size.height>612)
        image=[self imageByScalingAndCroppingForSize:image :612.0f:612.0f];
    img_data= UIImageJPEGRepresentation(image,0.5);
    
    return [self isConnected];
}
-(void)getProfileInfo{}

-(void) setSigned: (BOOL) sign{
    NSString * key = [NSString stringWithFormat:@"%@-Sign",socialId];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSNumber numberWithBool:sign] forKey:key];
    [defaults synchronize];
    is_Signed=sign;
}
-(void) setAppId: (NSString *) app {
    app_Id=app;
}

-(BOOL)checkSigned{
    if ([self isConnected]) {
        NSString * key = [NSString stringWithFormat:@"%@-Sign",socialId];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        is_Signed=[[defaults valueForKey:key] boolValue];
        return is_Signed;
    }
    return NO;
};

-(void) setToken: (NSString *) t {
    token=t;
}

-(void) setUse: (BOOL) use{
    is_Use=use;
    NSString * key = [NSString stringWithFormat:@"%@-Use",socialId];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSNumber numberWithBool:is_Use] forKey:key];
    [defaults synchronize];
    if (is_Use) {
        [self login];
    } else {
        [self logout];
    }
}


-(BOOL)isUse{
    NSString * key = [NSString stringWithFormat:@"%@-Use",socialId];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    is_Use=[[defaults valueForKey:key] boolValue];
    return is_Use;
};

-(BOOL)isSigned{
    return is_Signed;
};
-(NSString *)getUrlString{
    return url_String_Id;
};
-(void)setUrlString:(NSString*) url_id{
    url_String_Id=url_id;
};

-(BOOL)checkUrlString:(NSString*) url{
    return NO;
};

- (UIImage*)imageByScalingAndCroppingForSize :(UIImage *) sourceImage :(float)percent_reduction
{
    float w=sourceImage.size.width-(sourceImage.size.width*percent_reduction/100);
    float h=sourceImage.size.height-(sourceImage.size.height*percent_reduction/100);
    
    CGSize targetSize = CGSizeMake(w,h);
    
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (UIImage*)imageByScalingAndCroppingForSize :(UIImage *) sourceImage :(float )w :(float)h;
{
    
    CGSize targetSize = CGSizeMake(w,h);
    
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
        {
            scaleFactor = widthFactor; // scale to fit height
        }
        else
        {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
        {
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil)
    {
        NSLog(@"could not scale image");
    }
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end
