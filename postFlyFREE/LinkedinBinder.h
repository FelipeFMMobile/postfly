//
//  LinkedinBinder.h
//  postFlyFree
//
//  Created by Felipe Menezes on 28/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Binder.h"


#pragma LinkedIn

#import "OAuthLoginView.h"
#import "JSONKit.h"
#import "OAConsumer.h"
#import "OAMutableURLRequest.h"
#import "OADataFetcher.h"
#import "OATokenManager.h"


@interface LinkedinBinder : Binder {
    @private
        UIViewController * parentView;
        OAToken *requestToken;
        OAToken *accessToken;
        OAConsumer *consumer;
        
}


#pragma LinkedIn
@property (nonatomic, retain) OAuthLoginView *oAuthLoginView;

- (id)init;
- (id)initWithView:(UIViewController  *)view;
-(void)setViewLogin:(UIViewController  *) view;
-(void) loginViewDidFinish:(NSNotification*)notification;
- (void)profileApiCall;
- (void)postUpdateApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data;
- (void)profileApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data;
- (void)profileApiCallResult:(OAServiceTicket *)ticket didFail:(NSData *)error;

@end
