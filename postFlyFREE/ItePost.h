//
//  ItePost.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PostInfo;

@interface ItePost : NSManagedObject

@property (nonatomic, retain) NSString * rede_nome;
@property (nonatomic, retain) NSNumber * flag_sucess;
@property (nonatomic, retain) NSString * direct_url;
@property (nonatomic, retain) NSNumber * flag_image;
@property (nonatomic, retain) PostInfo *postinfo;

@end
