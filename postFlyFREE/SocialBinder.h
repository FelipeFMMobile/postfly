//
//  SocialBinder.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LinkedinBinder.h"
#import "FacebookBinder.h"
#import "GoogleBinder.h"
#import "TwitterBinder.h"
#import "PostInfo.h"


@interface SocialBinder : NSObject {
    
    @private
       BOOL flag_twitter_pass;
       NSString * recent_url_shortened;
       NSManagedObject * recent_post;
       
}

@property(retain,nonatomic) UIViewController* parentView;

@property(retain,nonatomic) FacebookBinder * facebook;
@property(retain,nonatomic) TwitterBinder * twitter;
@property(retain,nonatomic) LinkedinBinder * linkedin;
@property(retain,nonatomic) GoogleBinder * googleplus;

@property(retain,nonatomic) NSString * image_ref_url;
@property(retain,nonatomic) NSString * temp_message;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


- (id)init;

-(void)checkSign;

-(BOOL)postVerify:(NSString *)texto;
-(NSString *)callShortner:(NSString *)texto;
-(BOOL)isConnected;
-(BOOL)postFly:(NSString *) caption :(NSString *) message :(NSString *) link :(NSArray *) redes :(UIImage *)image;
-(BOOL)savePost:(NSString *) caption :(NSString *) message :(NSArray *) redes :(UIImage *)image;

-(NSString *) getRecentUrlShortned;

-(void)setRecentPost:(NSManagedObject *)post;

-(NSManagedObject *)getRecentPost;

-(NSMutableArray *)queryPosts;

-(void)setDelegateForBinders:(id)delegate;



@end
