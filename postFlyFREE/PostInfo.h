//
//  PostInfo.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PostInfo : NSManagedObject

@property (nonatomic, retain) NSDate * data;
@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * image_ref;
@property (nonatomic, retain) NSSet *itepost;
@end

@interface PostInfo (CoreDataGeneratedAccessors)

- (void)addItepostObject:(NSManagedObject *)value;
- (void)removeItepostObject:(NSManagedObject *)value;
- (void)addItepost:(NSSet *)values;
- (void)removeItepost:(NSSet *)values;

@end
