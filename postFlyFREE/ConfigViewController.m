//
//  ConfigViewController.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 10/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "ConfigViewController.h"
#import "FSTR.h"
#import "InfoViewController.h"
#import "IAPPurchaseViewController.h"

@interface ConfigViewController ()

@end

@implementation ConfigViewController
@synthesize _sFacebook;
@synthesize _sTwitter;
@synthesize _sLinkedin;
@synthesize _sGooglePlus;
@synthesize socialB;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_sFacebook setOn:[socialB.facebook isUse]];
    [_sTwitter setOn:[socialB.twitter isUse]];
    [_sLinkedin setOn:[socialB.linkedin isUse]];
    [_sGooglePlus setOn:[socialB.googleplus isUse]];
    
    [socialB setDelegateForBinders:self];
    self.title=[FSTR fstr:@"title_config"];
    if ([socialB isConnected]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults valueForKey:@"first"]==nil) {
            [defaults setFloat:1 forKey:@"first"];
            [defaults setFloat:3 forKey:@"free_limit"];
            [defaults  setBool:NO forKey:@"app_purchase"];
            [defaults synchronize];
     
            [IAPPurchaseViewController deleteKeyChain:nil];
           
        }
    }
    self.navigationController.toolbarHidden=YES;
    /*UIBarButtonItem * leftButton =  [[UIBarButtonItem alloc] initWithCustomView:_btVoltar];
    
    self.navigationItem.leftBarButtonItem = leftButton;
    */
}



-(IBAction)openInfo:(id)sender {
      if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
          InfoViewController * info = [[InfoViewController alloc] initWithNibName:@"InfoView-iPad" bundle:nil];
          [self.navigationController pushViewController:info animated:YES];
      } else {
          InfoViewController * info = [[InfoViewController alloc] initWithNibName:@"InfoView" bundle:nil];
          [self.navigationController pushViewController:info animated:YES];

      }
    
}

-(IBAction)selectSocial:(UISwitch *)sender {
    
    if (![socialB isConnected]) {
        [sender setOn:NO];
    }
    
    if (sender==_sFacebook) {
        socialB.facebook.temp_switch=sender;
        if (_sFacebook.isOn) [socialB.facebook setUse:YES];
        else [socialB.facebook setUse:NO];
    }
    
    if (sender==_sTwitter) {
        if (_sTwitter.isOn) [socialB.twitter setUse:YES];
        else [socialB.twitter setUse:NO];
    }
    
    if (sender==_sGooglePlus) {
     if (_sGooglePlus.isOn) [socialB.googleplus setUse:YES];
        else [socialB.googleplus setUse:NO];
    }
    
    if (sender==_sLinkedin) {
        [socialB.linkedin setViewLogin:self];
        if (_sLinkedin.isOn) [socialB.linkedin setUse:YES];
        else [socialB.linkedin setUse:NO];
    }
    
}
-(IBAction)sair:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)didLogin:(BOOL)sucess :socialID {
    
    if ([socialID isEqualToString:@"facebook"]) {
        if (!sucess) {
             [socialB.facebook setUse:NO];
            [_sFacebook setOn:[self.socialB.facebook isUse]];
        } else {
            [self.socialB.facebook reauthorizeForPublish];
        }
    }
    if ([socialID isEqualToString:@"twitter"]) {
        if (!sucess) {
            [socialB.twitter setUse:NO];
            [_sTwitter setOn:[self.socialB.twitter isUse]];
        }
    }
    if ([socialID isEqualToString:@"linkedin"]) {
        if (!sucess) {
            [socialB.linkedin setUse:NO];
            [_sLinkedin setOn:[self.socialB.linkedin isUse]];
        }
    }
    if ([socialID isEqualToString:@"googleplus"]) {
        if (!sucess) {
            [socialB.googleplus setUse:NO];
            [_sGooglePlus setOn:[self.socialB.googleplus isUse]];
        }
    }
    
}

- (void)viewDidUnload
{
    [self set_sFacebook:nil];
    [self set_sTwitter:nil];
    [self set_sLinkedin:nil];
    [self set_sGooglePlus:nil];

    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
