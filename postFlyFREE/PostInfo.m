//
//  PostInfo.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 09/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "PostInfo.h"


@implementation PostInfo

@dynamic data;
@dynamic caption;
@dynamic message;
@dynamic image_ref;
@dynamic itepost;

@end
