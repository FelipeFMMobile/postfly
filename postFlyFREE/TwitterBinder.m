//
//  TwitterBinder.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 05/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "TwitterBinder.h"
#import "FSTR.h"

@implementation TwitterBinder 
#define TWITTER_ @"twitter"


- (id)init
{
    if (self = [super initSocialId:TWITTER_]) {
        
        return self;
        
    }
    
    return nil;
}

-(BOOL)login{
    if ([super login]) {
        
        
        ACAccountStore *store = [[ACAccountStore alloc] init];
        ACAccountType *twitterAccountType =
        [store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        //  Request permission from the user to access the available Twitter accounts
        [store requestAccessToAccountsWithType:twitterAccountType
                         withCompletionHandler:^(BOOL granted, NSError *error) {
                             if (!granted) {
                                 // The user rejected your request
                                 NSLog(@"User rejected access to the account.");
                                 // delegate
                                 if ([delegate respondsToSelector:@selector(didLogin::)]) {
                                     [delegate didLogin:NO:TWITTER_];
                                 }
                             }
                             else {
                                 // Grab the available accounts
                                 NSArray *twitterAccounts =
                                 [store accountsWithAccountType:twitterAccountType];
                                 
                                 if ([twitterAccounts count] > 0) {
                                     // Use the first account for simplicity
                                     ACAccount *account = [twitterAccounts objectAtIndex:0];
                                     twitter_account=account;
                                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                     NSData *data = [NSKeyedArchiver archivedDataWithRootObject:account];
                                     [defaults setObject:data forKey:@"TwitterAccount"];
                                     [defaults synchronize];
                                     // delegate
                                     if ([delegate respondsToSelector:@selector(didLogin::)]) {
                                         [delegate didLogin:YES:TWITTER_];
                                     }
                                     [super setSigned:YES];
                                     [self getProfileInfo];

                                     
                                 } else {
                                     
                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[FSTR fstr:@"alert_twitter"]
                                                                                     message:[FSTR fstr:@"alert_twitter_message"]
                                                                                    delegate:nil
                                                                           cancelButtonTitle:[FSTR fstr:@"bt_Ok"]
                                                                           otherButtonTitles:nil];
                                     [alert show];
                                     // delegate
                                     if ([delegate respondsToSelector:@selector(didLogin::)]) {
                                         [delegate didLogin:NO:TWITTER_];
                                     }
                                 }// if ([twitterAccounts count] > 0)
                             } // if (granted)
                         }];

        
        return YES;
    }
    return NO;
    
}

-(BOOL)logout{
    [super logout];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:nil forKey:@"TwitterAccount"];
    [defaults synchronize];
    return YES;
    
}

-(BOOL)postWall :(NSString *) caption :(NSString *) message :(NSString *) link {
    
    if (twitter_account!=nil) {
    
        // Now make an authenticated request to our endpoint
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:message forKey:@"status"];
    
        //  The endpoint that we wish to call
        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
    
        //  Build the request with our parameter
        TWRequest *request = [[TWRequest alloc] initWithURL:url
                                                  parameters:params
                                                  requestMethod:TWRequestMethodPOST];
    
        // Attach the account object to this request
        [request setAccount:twitter_account];
    
        [request performRequestWithHandler:
                ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                        if (!responseData) {
                                // inspect the contents of error
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setBool:NO forKey:@"twitter_last_post"];
                            [defaults synchronize];
                            if ([delegate respondsToSelector:@selector(didPost::)]) {
                                    [delegate didPost:NO:TWITTER_];
                                }
                        }
                        else {
                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                            [defaults setBool:YES forKey:@"twitter_last_post"];
                            [defaults synchronize];
                            if ([delegate respondsToSelector:@selector(didPost::)]) {
                                [delegate didPost:YES:TWITTER_];
                            }
                        }
         }];
    }
    return YES;
}

-(BOOL)postImage :(NSString *) caption :(NSString *) message :(UIImage *)image :(NSString *) link{
    if (twitter_account!=nil) {
        
      //  NSLog(@" Original Image Size %f , %f",image.size.width,image.size.height);
   
        // reducao da imagem
        if (image.size.width>612 && image.size.height>612)
            image=[self imageByScalingAndCroppingForSize:image :612.0f:612.0f];
        //}
 //        NSLog(@" REDUCED Image Size %f , %f",image.size.width,image.size.height);
        // Now make an authenticated request to our endpoint
        //  Obtain NSData from the UIImage
        NSData *imageData = UIImageJPEGRepresentation(image,0.5);
       
        //  The endpoint that we wish to call
        NSURL *url = [NSURL URLWithString:@"https://upload.twitter.com/1/statuses/update_with_media.json"];
        
        //  Build the request with our parameter
        TWRequest *request = [[TWRequest alloc] initWithURL:url
                                                 parameters:nil
                                              requestMethod:TWRequestMethodPOST];
        
        // Attach the account object to this request
        [request setAccount:twitter_account];
        
      
        //  Add the data of the image with the
        //  correct parameter name, "media[]"
        [request addMultiPartData:imageData
                         withName:@"media[]"
                             type:@"multipart/form-data"];
        
        NSString *status = message;
        
        [request addMultiPartData:[status dataUsingEncoding:NSUTF8StringEncoding]
                         withName:@"status"
                             type:@"multipart/form-data"];
        
        [request performRequestWithHandler:
         ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
            if (!responseData) {
                 // inspect the contents of error
                 if ([delegate respondsToSelector:@selector(didPost::)]) {
                     [delegate didPost:NO:TWITTER_];
                 }
             }
             else {
                 NSString *responseBody = [[NSString alloc] initWithData:responseData
                                                                encoding:NSUTF8StringEncoding];
                 responseBody=[responseBody stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                 responseBody=[responseBody stringByReplacingOccurrencesOfString:@"<" withString:@""];
                 responseBody=[responseBody stringByReplacingOccurrencesOfString:@">" withString:@""];
                // NSLog(@"Retorno %@",responseBody);
                 if ([@"over capacity" rangeOfString:responseBody].location != NSNotFound) {
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setBool:NO forKey:@"twitter_last_post"];
                     [defaults synchronize];
                     if ([delegate respondsToSelector:@selector(didPost::)]) {
                         [delegate didPost:NO:TWITTER_];
                     }
                 } else {
                     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setBool:YES forKey:@"twitter_last_post"];
                     [defaults synchronize];
                     if ([delegate respondsToSelector:@selector(didPost::)]) {
                         [delegate didPost:YES:TWITTER_];
                     }
                 }
             }
         }];
    }
    return YES;

}

// esta chamada é executada quando a app já foi autenticada uma vez, evitando chamar a tela de login a todo tempo.
-(BOOL)checkSigned{
    if ([super checkSigned]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSData *data = [defaults objectForKey:@"TwitterAccount"];
        twitter_account = (ACAccount *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
        
        //twitter_account=[defaults valueForKey:@"TwitterAccount"];
        [self getProfileInfo];
        return YES;
    } else {
        [self login];
    }
    return NO;
}

-(void)getProfileInfo{
    if (twitter_account!=nil) {
        self.profile_name=[twitter_account username];
        self.profile_info= [twitter_account description];
        if ([delegate respondsToSelector:@selector(didGetProfile::)]) {
            [delegate didGetProfile:YES:TWITTER_];
        }
    }
    
}


@end
