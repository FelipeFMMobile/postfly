//
//  FacebookBinder.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 03/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "FacebookBinder.h"

#import "AppDelegate.h"
#import "FSTR.h"



@implementation FacebookBinder
#define FACEBOOK_ @"facebook"
#define URL_ID_ @"fb227943977331014"



#pragma metodos customizados

- (id)init
{
    if (self = [super initSocialId:FACEBOOK_]) {
        [self setUrlString:URL_ID_];
        
        return self;
        
    }
    
    return nil;
}





-(BOOL)login{
    if ([super login]) {
        
         
          //  NSArray *permissions = [NSArray arrayWithObjects:@"user_photos", @"publish_stream",@"publish_actions",@"status_update",nil];
          NSArray *permissions = [NSArray arrayWithObjects:@"user_birthday",@"user_photos",@"status_update",nil];
        
            [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState state,
                                                          NSError *error) {
                                          [self sessionStateChanged:session
                                                              state:state
                                                              error:error];
                                              //NSLog(@"Ocorreu erro : %@",error);
                                        }];
        
        return YES;
    }
    return NO;
    
}

-(BOOL)logout{
    [super logout];
  [FBSession.activeSession  closeAndClearTokenInformation];
    
    return YES;
    
}
// handle session issues and confirmation
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
      switch (state) {
        case FBSessionStateOpen:
            if (!error) {
               [super setSigned:YES];
                // implementação para o IOS6
                 [self getProfileInfo];
                
                // delegate com resultado do login
               if ([delegate respondsToSelector:@selector(didLogin::)]) {
                    [delegate didLogin:YES:FACEBOOK_];
                }
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            // NSLog(@"User Error session");
            [FBSession.activeSession closeAndClearTokenInformation];
            [super setSigned:NO];
            // delegate com resultado do login
            if ([delegate respondsToSelector:@selector(didLogin::)]) {
                [delegate didLogin:NO:FACEBOOK_];
            }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:FBSessionStateChangedNotification
     object:session];

    /*if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }*/
    if (error) {
        
        
        NSString *errorTitle = NSLocalizedString(@"Error", [FSTR fstr:@"alert_title_faceerror"]);
        NSString *errorMessage = [error localizedDescription];
        if (error.code == FBErrorLoginFailedOrCancelled) {
            errorTitle = NSLocalizedString([FSTR fstr:@"alert_title_faceerrorlogin"], [FSTR fstr:@"alert_title_faceerrorconnect"]);
            errorMessage = NSLocalizedString([FSTR fstr:@"alert_title_faceerrorsetting"], @"Facebook connect");
            [_temp_switch setOn:NO];
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:errorTitle
                                                            message:errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"Facebook Connect")
                                                  otherButtonTitles:nil];
        
        [alertView show];
    }

}

// esta chamada é executada quando a app já foi autenticada uma vez, evitando chamar a tela de login a todo tempo.
-(BOOL)checkSigned{
    if ([super checkSigned]) {
       // if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
       
        //
        if (!FBSession.activeSession.isOpen) {
            [self login];
            return YES;
        } else {
             [self getProfileInfo];
             return YES;
        }
    }
     return NO;
}
// use Reauthorize por causa do IOS6 que pede para fazer duas autorizações separadas.
-(BOOL) reauthorizeForPublish {
    NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions",nil];
    
    // Ask for publish_actions permissions in context
    if ([FBSession.activeSession.permissions
         indexOfObject:@"publish_actions"] == NSNotFound) {
        // No permissions found in session, ask for it
        [FBSession.activeSession
         reauthorizeWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends
         completionHandler:^(FBSession *session, NSError *error) {
             if (!error) {  }
         }];
        return YES;
    } 
    return NO;
}

-(BOOL)postWall:(NSString *) caption :(NSString *) message :(NSString *) link {
    
    if ([super postWall:caption :message:link] && FBSession.activeSession.isOpen) {
        
        NSMutableDictionary * postParams;
        if (caption==nil && link==nil)  {
            postParams =
            [[NSMutableDictionary alloc] initWithObjectsAndKeys:
             @"postFly App", @"name",
             message, @"message",
             nil];
        } else {
            postParams =
            [[NSMutableDictionary alloc] initWithObjectsAndKeys:
             link, @"link",
             @"postFly App", @"name",
             caption, @"caption",
             message, @"message",
             nil];
        }
        
        [FBRequestConnection startWithGraphPath:@"me/feed" parameters:postParams HTTPMethod:@"POST"
                       completionHandler:^(FBRequestConnection *connection,
                                    id result,
                             NSError *error) {
                                    if (error) {
                                        NSLog(@"Error %@",error);
                                        // delegate
                                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                        [defaults setBool:NO forKey:@"facebook_last_post"];
                                        [defaults synchronize];
                                        if ([delegate respondsToSelector:@selector(didPost::)]) {
                                            [delegate didPost:NO:FACEBOOK_];
                                        }
                                    } else {
                                        // delegate
                                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                        [defaults setBool:YES forKey:@"facebook_last_post"];
                                        [defaults synchronize];
                                        if ([delegate respondsToSelector:@selector(didPost::)]) {
                                            [delegate didPost:YES:FACEBOOK_];
                                        }
                                   }

         }];
        return YES;
        
    }
    return NO;
}

-(BOOL)postImage:(NSString *) caption :(NSString *) message :(UIImage *)image :(NSString *) link{
    if ([super postImage:caption :message :image :link]) {
       
        
        //NSLog(@" Original Image Size %f , %f",image.size.width,image.size.height);
         // reducao da imagem
         if (image.size.width>612 && image.size.height>612)
             image=[self imageByScalingAndCroppingForSize:image :612.0f:612.0f];
        // NSLog(@" REDUCED Image Size %f , %f",image.size.width,image.size.height);
        
        // força a redução da imagem dentro do código.
        NSData *imageData = UIImageJPEGRepresentation(image,0.5);
        image=nil;
        image=[UIImage imageWithData:imageData];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       image, @"source",
                                       message, @"message",
                                       nil];
 
        [FBRequestConnection startWithGraphPath:@"me/photos" parameters:params HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection,                                                                                                                    id result,                                                                                                                    NSError *error) {
                                  // NSLog(@" Result %@",result);
                                  if (!error) {
                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                      [defaults setBool:YES forKey:@"facebook_last_post"];
                                      [defaults synchronize];
                                      if ([delegate respondsToSelector:@selector(didPost::)]) {
                                          [delegate didPost:YES:FACEBOOK_];
                                      }
                                      
                                  } else {
                                      NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                      [defaults setBool:NO forKey:@"facebook_last_post"];
                                      [defaults synchronize];
                                      if ([delegate respondsToSelector:@selector(didPost::)]) {
                                          [delegate didPost:NO:FACEBOOK_];
                                      }
                                  }
                                  if (result!=nil) {
                                      if ([result valueForKey:@"id"]!=nil) {
                                          NSString * postid=[NSString stringWithFormat:@"%@",[result valueForKey:@"id"]];
                                          [FBRequestConnection startWithGraphPath:postid                                                                           completionHandler:^(FBRequestConnection *connection,                                                                                                                    id result,                                                                                                                    NSError *error) {
                                              if (result!=nil) {
                                                  NSString *source = [result objectForKey:@"source"];
                                                  if ([delegate respondsToSelector:@selector(didPostImageWithUrl::)]) {
                                                    [delegate didPostImageWithUrl:YES :source];
                                                  }
                                              }
                                            }];
                                      }
                                  }
                                  
                                    }];
      
      return YES;
    }
    
    return NO;
}




-(void)getProfileInfo{
    
    
  //  if (FBSession.activeSession.isOpen) {
        FBRequest *me = [FBRequest requestForMe];
        [me startWithCompletionHandler: ^(FBRequestConnection *connection,
                                          NSDictionary<FBGraphUser> *my,
                                          NSError *error) {
            if (!error) {
                    self.profile_name=my.name;
                    self.profile_info=[NSString stringWithFormat:@"%@ %@ %@",my.first_name,my.middle_name,my.last_name];
                  //NSLog(@"Link %@",my.link);
                  self.profile_wall_url=my.link;
            
                self.profile_id=my.id;
                NSString *fbuid = my.username;
                // pegando a imagem profile no facebook
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?", fbuid]];
                NSData *data = [NSData dataWithContentsOfURL:url];
                UIImage *profilePic = [[UIImage alloc] initWithData:data];
                self.profile_image=profilePic;
                // delegate
                if ([delegate respondsToSelector:@selector(didGetProfile::)]) {
                    [delegate didGetProfile:YES:FACEBOOK_];
                }
            } else {
                if ([delegate respondsToSelector:@selector(didGetProfile::)]) {
                    [delegate didGetProfile:NO:FACEBOOK_];
                }
            }
        }];
 /*   } else {
        // delegate
        if ([delegate respondsToSelector:@selector(didGetProfile::)]) {
            [delegate didGetProfile:NO:FACEBOOK_];
        }
        //[self login];
    }*/

}

-(BOOL)checkUrlString:(NSString*) url{
    return ([url rangeOfString:[self getUrlString]].location != NSNotFound);
}

@end
