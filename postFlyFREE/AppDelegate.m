//
//  AppDelegate.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 30/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "Reachability.h"
#import "TFTTapForTap.h"

@interface AppDelegate (PrivateMethods)

-(void)loadStart;

@end

@implementation AppDelegate 

NSString *const FBSessionStateChangedNotification =
@"com.fmmobile.postFlyFREE:FBSessionStateChangedNotification";




@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize socialB;
@synthesize mainViewController;
@synthesize _navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.socialB=[[SocialBinder alloc] init];
    
    [TFTTapForTap initializeWithAPIKey:@"9b67589ae2f4a944fff059afc4ed72c2"];
    
    [self loadStart];
     
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    
    return YES;
}

-(void)loadStart {

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.mainViewController = [[MainViewController alloc] initWithNibName:@"MainView-iPad" bundle:nil];
    } else {
        self.mainViewController = [[MainViewController alloc] initWithNibName:@"MainView" bundle:nil];
    }
    
    self.mainViewController.socialB=self.socialB;
    
    UINavigationController * navigationController = [[UINavigationController alloc]
                                                     initWithRootViewController:self.mainViewController];
    _navigationController=navigationController;
    _navigationController.toolbar.tintColor=[UIColor colorWithRed:0.0 green:0.4 blue:0.5 alpha:1.0];
    _navigationController.navigationBar.tintColor =  [UIColor colorWithRed:0.0 green:0.4 blue:0.5 alpha:1.0];

    self.window.rootViewController = self._navigationController;
}

- (void)didPostImageWithUrl :(BOOL) sucess :(NSString *)url {
        // completa a acao para outras midias sociais, que nao tem como fazer upload de foto.
         // NSLog(@"Post em IMAGEM BACKGROUND %@ ",url);
      [self.socialB.facebook postWall:self.socialB.temp_message :self.socialB.temp_message :url];
        NSManagedObject * post=[self.socialB getRecentPost];
        
        NSArray * itens = [[post valueForKey:@"itepost"] allObjects];
        
        for (NSString * item  in itens) {
            
            NSString * rede = [item valueForKey:@"rede_nome"];
            
            if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
                    [self.socialB.linkedin postWall:nil:self.socialB.temp_message :url];
             }
            
        } //for
   
}
-(void)didPost:(BOOL)sucess :(NSString *)socialID {
    
    NSManagedObject * post=[socialB getRecentPost];
    
    NSArray * itens = [[post valueForKey:@"itepost"] allObjects];
    
    for (NSString * item  in itens) {
        
        NSString * rede = [item valueForKey:@"rede_nome"];
        
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
            if ([@"facebook" rangeOfString:socialID].location != NSNotFound) {
                [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults removeObjectForKey:@"facebook_last_post"];
                [defaults synchronize];
            }
        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            if ([@"twitter" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults removeObjectForKey:@"twitter_last_post"];
                [defaults synchronize];
            }
        }
        if ([@"linkedin" rangeOfString:rede].location != NSNotFound) {
            if ([@"linkedin" rangeOfString:socialID].location != NSNotFound) {
                    [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
            }
        }
        if ([@"googleplus" rangeOfString:rede].location != NSNotFound) {
            if ([@"googleplus" rangeOfString:socialID].location != NSNotFound) {
                   [item setValue:[NSNumber numberWithBool:sucess] forKey:@"flag_sucess"];
            }
        }
    }
    
    
    NSError *error = nil;
    if (![self.socialB.managedObjectContext save:&error]) {
      //  NSLog(@"Error %@",error);
    }
   // NSLog(@"Post em BACKGROUND %@ realizado %i",socialID,sucess);
}

#pragma Facebook & Google+ API Return

- (BOOL)application:(UIApplication *)application  openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
#pragma mark - GooglePlus API
    if ([self.socialB.googleplus checkUrlString:[url URLStringWithoutQuery]]) {
        if (socialB.googleplus.viewController!=nil) {
            UIViewController * v = (UIViewController *)socialB.googleplus.viewController;
            [v dismissViewControllerAnimated:YES completion:nil];
            if ([url.absoluteString rangeOfString:@"canceled"].location != NSNotFound) {
                if ([socialB.googleplus.delegate respondsToSelector:@selector(didPost::)]) {
                    [socialB.googleplus.delegate didPost:NO:GOOGLEP_];
                }
            } else {
                if ([socialB.googleplus.delegate respondsToSelector:@selector(didPost::)]) {
                    [socialB.googleplus.delegate didPost:YES:GOOGLEP_];
                }
            }
        }
      //  NSLog(@"Retorno do Google");
        return YES;
    }

    #pragma mark - Facebook API
    // detectar se retorno vem do facebook login
    if ([self.socialB.facebook checkUrlString:[url URLStringWithoutQuery]]) {
       // NSLog(@"Retorno do Facebook");
        return [FBSession.activeSession handleOpenURL:url];
    }

    return NO;
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [socialB setDelegateForBinders:self];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        self.window.rootViewController=nil;
    }
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if (self.window.rootViewController==nil) {
        self.window.rootViewController =self._navigationController;
        [self.mainViewController  setAgainCheckinl:YES];
        [self._navigationController popToRootViewControllerAnimated:NO];
    }
    // We need to properly handle activation of the application with regards to SSO
    // (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    [FBSession.activeSession handleDidBecomeActive];
    
    // verifica o callback mesmo se a app foi para background por muito tempo.
    // o objetivo deste bloco é atualizar o callback quando o usuário voltar a app,
    // com ob jetivo de reduzir erros de callback
   /* NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSManagedObject * recent_post=[defaults valueForKey:@"recent_post_object"];
    NSArray * itens = [[recent_post valueForKey:@"itepost"] allObjects];
    
    for (NSString * item  in itens) {
        
        NSString * rede = [item valueForKey:@"rede_nome"];
        
        if ([@"facebook" rangeOfString:rede].location != NSNotFound) {
            if ([defaults valueForKey:@"facebook_last_post"]!=nil) {
                     [item setValue:[NSNumber numberWithBool:[defaults boolForKey:@"facebook_last_post"]] forKey:@"flag_sucess"];
            }
        }
        if ([@"twitter" rangeOfString:rede].location != NSNotFound) {
            if ([defaults valueForKey:@"twitter_last_post"]!=nil) {
                [item setValue:[NSNumber numberWithBool:[defaults boolForKey:@"twitter_last_post"]] forKey:@"flag_sucess"];
            }
        }
        
    }
    NSError *error = nil;
    if (![self.socialB.managedObjectContext save:&error]) {
      //  NSLog(@"Error %@",error);
    }*/
    
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [FBSession.activeSession close];
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
           // NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"postFlyFREE" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"postFlyFREE.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
     //   NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
