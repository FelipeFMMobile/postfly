//
//  LinkedinBinder.m
//  postFlyFree
//
//  Created by Felipe Menezes on 28/09/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "LinkedinBinder.h"
#import "FSTR.h"

@implementation LinkedinBinder

#define LINKEDIN_ @"linkedin"
#define APP_KEY_ @"nv9qxow90kfy"
#define APP_SECRET_ @"10MS5iHx78mG9JBV"


@synthesize oAuthLoginView;
- (id)init
{
    if (self = [super initSocialId:LINKEDIN_]) {
        
        return self;
        
    }

    
    return nil;
}
-(void)setViewLogin:(UIViewController *) view {
    parentView=view;
}
- (id)initWithView:(UIViewController *)viewcontroller
{
    if (self = [super initSocialId:LINKEDIN_]) {
        parentView=viewcontroller;
        
        return self;
        
    }
    
    return nil;
}

#pragma metodos customizados

-(BOOL)login{
    if ([super login]) {
        if (parentView!=nil) {
            oAuthLoginView = [[OAuthLoginView alloc] initWithNibName:nil bundle:nil];
            [[self oAuthLoginView] setAppKeySecret:APP_KEY_ : APP_SECRET_ ];
            // register to be told when the login is finished
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(loginViewDidFinish:)
                                                     name:@"loginViewDidFinish"
                                                     object:oAuthLoginView];
            [parentView presentModalViewController:oAuthLoginView animated:YES];
            
            return YES;
        }
    }
    return NO;
  
}

-(BOOL)logout{
    [super logout];
    [OAToken removeFromUserDefaultsWithServiceProviderName:@"linkedin"  prefix:@"token"];
    [OAToken removeFromUserDefaultsWithServiceProviderName:@"linkedin"  prefix:@"request"];
    return YES;
}


// return ok from oAuthView 
-(void) loginViewDidFinish:(NSNotification*)notification
{
    [OAToken removeFromUserDefaultsWithServiceProviderName:@"linkedin"  prefix:@"token"];
    [OAToken removeFromUserDefaultsWithServiceProviderName:@"linkedin"  prefix:@"request"];
    [oAuthLoginView.accessToken storeInUserDefaultsWithServiceProviderName:@"linkedin" prefix:@"token"];
    [oAuthLoginView.requestToken storeInUserDefaultsWithServiceProviderName:@"linkedin" prefix:@"request"];

    // delegate
    if ([delegate respondsToSelector:@selector(didLogin::)]) {
        [delegate didLogin:YES:LINKEDIN_];
    }
	[[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super setSigned:YES];
    [self getProfileInfo];
	
}

// esta chamada é executada quando a app já foi autenticada uma vez, evitando chamar a tela de login a todo tempo. 
-(BOOL)checkSigned{
    if ([super checkSigned]) {
        accessToken= [[OAToken alloc] initWithUserDefaultsUsingServiceProviderName:@"linkedin" prefix:@"token"];
        requestToken=[[OAToken alloc] initWithUserDefaultsUsingServiceProviderName:@"linkedin" prefix:@"request"];
        consumer = [[OAConsumer alloc] initWithKey:APP_KEY_
                                            secret:APP_SECRET_
                                                  realm:@"http://api.linkedin.com/"];
        
        if ([accessToken hasExpired]  || [requestToken hasExpired]) {
            [self login];
            return NO;
        }
        /*
        NSData *data1 = [defaults objectForKey:@"LinkConsumer"];
        consumer= (OAConsumer*)[NSKeyedUnarchiver unarchiveObjectWithData:data1];
        
        NSData *data2 = [defaults objectForKey:@"LinkRequest"];
        requestToken= (OAToken*)[NSKeyedUnarchiver unarchiveObjectWithData:data2];
     */
          if (accessToken==nil || consumer==nil || requestToken==nil) return NO;
          [self getProfileInfo];
          return YES;
      } /*else {
          [self login];
      }*/
    
    return NO;
}


-(void)getProfileInfo{
    
    [self profileApiCall];
}


-(BOOL)postWall:(NSString *) caption :(NSString *) message :(NSString *) link {
    if ([super postWall:caption :message:link]) {
    
    if (link!=nil) message=[NSString stringWithFormat:@"%@ %@",message,link];
    
    NSURL *url = [NSURL URLWithString:@"http://api.linkedin.com/v1/people/~/shares"];
    OAMutableURLRequest *request =
    [[OAMutableURLRequest alloc] initWithURL:url
                                    consumer:consumer
                                       token:accessToken
                                    callback:nil
                           signatureProvider:nil];
    
    NSDictionary *update = [[NSDictionary alloc] initWithObjectsAndKeys:
                            [[NSDictionary alloc]
                             initWithObjectsAndKeys:
                             @"anyone",@"code",nil], @"visibility",
                            message, @"comment",nil];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *updateString = [update JSONString];

    [request setHTTPBodyWithString:updateString];
	[request setHTTPMethod:@"POST"];
    
    OADataFetcher *fetcher = [[OADataFetcher alloc] init];
    [fetcher fetchDataWithRequest:request
                         delegate:self
                didFinishSelector:@selector(postUpdateApiCallResult:didFinish:)
                  didFailSelector:@selector(postUpdateApiCallResult:didFail:)];
        return YES;
    }
    return NO;
}

- (void)postUpdateApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data
{
   
    // delegate
    NSString *responseBody = [[NSString alloc] initWithData:data
                                                   encoding:NSUTF8StringEncoding];
    //NSLog(@"Retorno %@",responseBody);
    if ([responseBody length]>5)
    {
        if ([delegate respondsToSelector:@selector(didPost::)]) {
            [delegate didPost:YES:LINKEDIN_];
        }
    }  else {
        if ([delegate respondsToSelector:@selector(didPost::)]) {
            [delegate didPost:NO:LINKEDIN_];
        }
    }
    
}

- (void)postUpdateApiCallResult:(OAServiceTicket *)ticket didFail:(NSData *)error
{
    // delegate
    if ([delegate respondsToSelector:@selector(didPost::)]) {
        [delegate didPost:NO:LINKEDIN_];
    }
}



#pragma metodos complementares

- (void)profileApiCall
{
    NSURL *url = [NSURL URLWithString:@"http://api.linkedin.com/v1/people/~"];
    OAMutableURLRequest *request =
    [[OAMutableURLRequest alloc] initWithURL:url
                                    consumer:consumer
                                       token:accessToken
                                    callback:nil
                           signatureProvider:nil];
    
    [request setValue:@"json" forHTTPHeaderField:@"x-li-format"];
    
    OADataFetcher *fetcher = [[OADataFetcher alloc] init];
    [fetcher fetchDataWithRequest:request
                         delegate:self
                didFinishSelector:@selector(profileApiCallResult:didFinish:)
                  didFailSelector:@selector(profileApiCallResult:didFail:)];
    
    
}

- (void)profileApiCallResult:(OAServiceTicket *)ticket didFinish:(NSData *)data
{
    NSString *responseBody = [[NSString alloc] initWithData:data
                                                   encoding:NSUTF8StringEncoding];
    
    NSDictionary *profile = [responseBody objectFromJSONString];
    
    
    if ( profile )
    {
        NSString * name  = [[NSString alloc] initWithFormat:@"%@ %@",
                            [profile objectForKey:@"firstName"], [profile objectForKey:@"lastName"]];
        NSString * headline = [profile objectForKey:@"headline"];
        NSJSONSerialization * url = [profile objectForKey:@"siteStandardProfileRequest"] ;
       
        NSString * u = [url valueForKey:@"url"];
        
        
        
        self.profile_name=name;
        self.profile_info=headline;
        self.profile_wall_url=u;
        // delegate
        if ([delegate respondsToSelector:@selector(didGetProfile::)]) {
            [delegate didGetProfile:YES:LINKEDIN_];
        }
    }
    
    
}

- (void)profileApiCallResult:(OAServiceTicket *)ticket didFail:(NSData *)error
{
    [super setSigned:NO];
    // delegate
    if ([delegate respondsToSelector:@selector(didGetProfile::)]) {
        [delegate didGetProfile:NO:LINKEDIN_];
    }
    //NSLog(@"%@",[error description]);
}



@end
