//
//  TwitterBinder.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 05/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "Binder.h"

@interface TwitterBinder : Binder {
@private
    ACAccount * twitter_account;
}

- (id)init;

@end
