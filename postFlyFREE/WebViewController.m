//
//  WebViewController.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 16/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import "WebViewController.h"
#import "FSTR.h"

@interface WebViewController ()

@end

@implementation WebViewController
@synthesize _webview;
@synthesize _btVoltar;
@synthesize _navbar;
@synthesize _navitem;
@synthesize _label_wait;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem * leftButton =  [[UIBarButtonItem alloc] initWithCustomView:self._btVoltar];
    
    self._navitem.leftBarButtonItem = leftButton;
    
    self._navbar.topItem.title=[FSTR fstr:@"title_web"];
    self._webview.delegate=self;

}
- (void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
 
    self._webview.hidden=YES;
    
    self._label_wait.text=[FSTR fstr:@"wait_web_message"];
    
    [self._webview reload];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
   self._webview.hidden=NO;
    
}

- (void)viewDidUnload
{
    [self set_webview:nil];
    [self set_btVoltar:nil];
    [self set_navbar:nil];
    [self set_navitem:nil];
    [self set_label_wait:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)sair:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
