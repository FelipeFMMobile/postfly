//
//  LogWebViewController.h
//  postFlyFREE
//
//  Created by Felipe Menezes on 12/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocialBinder.h"
#import <AssetsLibrary/AssetsLibrary.h>



typedef void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *asset);
typedef void (^ALAssetsLibraryAccessFailureBlock)(NSError *error);

@interface LogViewController : UIViewController<BinderDelegate> {
    
    BOOL selFacebook;
    BOOL selTwitter;
    BOOL selLinkedin;
    BOOL selGoogle;
    BOOL is_facebook_image;

}


@property (strong, nonatomic) IBOutlet UILabel *_labelPost;
@property (strong, nonatomic) IBOutlet UIButton *_btFacebook;
@property (strong, nonatomic) IBOutlet UIButton *_btTwitter;
@property (strong, nonatomic) IBOutlet UIButton *_btLinkedin;
@property (strong, nonatomic) IBOutlet UIButton *_btGoogle;
@property (strong, nonatomic) IBOutlet UILabel *_labelData;
@property (strong, nonatomic) IBOutlet UIButton *_getStore;
@property (strong,nonatomic) NSArray * itemPost;
@property (strong,nonatomic) SocialBinder * socialB;
@property (strong, nonatomic) NSString *imageref;
@property (strong, nonatomic) IBOutlet UIButton *_bt_resend;

@property (strong, nonatomic) NSManagedObject *post;

@property (strong, nonatomic) IBOutlet UIImageView *_imagepost;

@property (strong, nonatomic) IBOutlet UIImageView *_back_image_post;


@property (strong,nonatomic) NSString * message;
@property (strong,nonatomic) NSDate * data;

-(IBAction)getFull:(id)sender;
-(IBAction)reSend:(id)sender;
-(void)setButtons;


@end
