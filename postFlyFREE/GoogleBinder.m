//
//  GoogleBinder.m
//  postFlyFREE
//
//  Created by Felipe Menezes on 04/10/12.
//  Copyright (c) 2012 Felipe Menezes. All rights reserved.
//

//#import "GoogleBinder.h"
//#import "GooglePlusSignIn.h"
//#import "GPPSignIn.h"
#import "AppDelegate.h"
#import "GTLPlusConstants.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GPPShare.h"

@implementation GoogleBinder

#define APP_CLIENT_ID_ @"522095909827.apps.googleusercontent.com"
#define APP_SECRET @"t5SZp7qkKfcHZjKyvf0e4G-U"
#define URL_ID_ @"com.fmmobile.postFlyFREE"
static NSString *const kKeychainItemName = @"PostFly Free G+";

#pragma metodos customizados

- (id)init
{
    if (self = [super initSocialId:GOOGLEP_]) {
        [self setUrlString:URL_ID_];
        
        return self;
        
    }
    
    return nil;
}

-(BOOL)login  {
    if ([super login]) {
      //** O AUTH2 LOGIN TESTE AUTENTICATION
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
      NSString * at=[defaults valueForKey:@"google"];
        if (at!=nil) {
            GTMOAuth2Authentication *auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName clientID:APP_CLIENT_ID_ clientSecret:APP_SECRET];
            [auth setKeysForPersistenceResponseString:at];
            auth.accessToken=[defaults valueForKey:@"googletoken"];
            if (auth!=nil) {
                [self correctLogin:auth];
                return YES;
            }
        }
        
        NSString *kMyClientID = APP_CLIENT_ID_;     // pre-assigned by service
        NSString *kMyClientSecret = APP_SECRET; // pre-assigned by service
        NSString *scope = @"https://www.googleapis.com/auth/plus.me"; // scope for Google+ API
        
        GTMOAuth2ViewControllerTouch *authView;
         authView = [[GTMOAuth2ViewControllerTouch alloc]  initWithScope:scope
                                                                     clientID:kMyClientID
                                                                 clientSecret:kMyClientSecret
                                                                
                                                             keychainItemName:kKeychainItemName
                                                                     delegate:self
                                                             finishedSelector:@selector(viewController:finishedWithAuth:error:)];
        
        [_mainView presentViewController:authView animated:YES completion:nil];
         
            
        return YES;
    }
    return NO;
    
}
- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error {
    [viewController dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"Autorizado : %@",auth);
    if (!error) {
        _viewController=viewController;
        [self correctLogin:auth];
        return;
    }
    // delegate
    if ([delegate respondsToSelector:@selector(didLogin::)]) {
        [delegate didLogin:NO:GOOGLEP_];
    }

}

-(BOOL)logout{
    if ([super logout]) {
        //[[GPPSignIn sharedInstance] signOut];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults removeObjectForKey:@"google"];
        [defaults synchronize];
        /*GooglePlusSignIn *signIn =
        [[GooglePlusSignIn alloc] initWithClientID:APP_CLIENT_ID_
                                          language:@"en"
                                             scope:[NSArray arrayWithObjects:
                                                    @"https://www.googleapis.com/auth/plus.moments.write",
                                                    @"https://www.googleapis.com/auth/plus.me",
                                                    nil]
                                      keychainName:nil];
        
        [signIn signOut];     */   
        return YES;
    }
    return NO;
    
}

-(void) correctLogin :(GTMOAuth2Authentication *)auth{
    // persistindo o auth
    _mainAuth=auth;
    NSString * at = [auth persistenceResponseString];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:at forKey:@"google"];
    
    [defaults setValue:auth.accessToken forKey:@"googletoken"];
    [defaults synchronize];
     // delegate
    if ([delegate respondsToSelector:@selector(didLogin::)]) {
        [delegate didLogin:YES:GOOGLEP_];
    }
    [super setSigned:YES];
    [self getProfileInfo];
}


// esta chamada é executada quando a app já foi autenticada uma vez, evitando chamar a tela de login a todo tempo.
-(BOOL)checkSigned{
    if ([super checkSigned]) {
        [self login];
        return YES;
    } else {
         [self login];
    }
    return NO;
}
-(BOOL)postWall:(NSString *) caption :(NSString *) message :(NSString *) link {
    
    message=[message stringByAddingPercentEscapesUsingEncoding:kCFStringEncodingUTF8];
    
    self.viewController=[[GTMOAuth2ViewControllerTouch alloc] initWithNibName:@"GTMOAuth2ViewTouch" bundle:nil];
    NSString * urlshare= [NSString stringWithFormat:@"https://plus.google.com/share?user_id=%@&continue=com.fmmobile.postFlyFREE://share/&text=%@&bundle_id=com.fmmobile.postFlyFREE&client_id=%@&auth_token=%@&hl=pt&gpsdk=1.2.1&gpsrc=frameless&partnerid=frameless",
                          _mainAuth.userID,message,APP_CLIENT_ID_,_mainAuth.accessToken];
    //NSLog(@" URL : %@",urlshare);
     NSURL *url = [NSURL URLWithString:urlshare];
    [_mainView presentViewController:_viewController animated:YES completion:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [_viewController.webView loadRequest:request];
    
    return NO;
}

-(BOOL)checkUrlString:(NSString*) url{
    NSString * url2 =[[self getUrlString] lowercaseString];
    if (([url rangeOfString:url2].location != NSNotFound)) {
            [super setSigned:YES];
            [self getProfileInfo];
            return YES;
    }
    return NO;
}

@end
